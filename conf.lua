function love.conf(t)
	t.identity = "tromi_mobile"
	t.externalstorage=true

	t.console = true

	t.window.title = "Tromi"
	t.window.width = 1280
	t.window.height = 960
	t.window.vsync = false

	t.accelerometerjoystick = false
end
