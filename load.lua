-- Fonts
FONT_tromi = love.graphics.newFont('res/fonts/Iosevka-Bold.ttf' , 28)
FONT_big   = love.graphics.newFont('res/fonts/Iosevka-Heavy.ttf', 56)
-- Icons
FONT_tromi:setFallbacks(love.graphics.newFont('res/fonts/techmino_proportional.otf', 28))
FONT_big  :setFallbacks(love.graphics.newFont('res/fonts/techmino_proportional.otf', 56))
CHAR = require("char")

local font_height = FONT_tromi:getHeight() * 0.5
local font_big_height = FONT_big:getHeight() * 0.5

-- Bigint library
bigint = require "libs.bigint.bigint"
number_names = require "libs.bigint.named-powers-of-ten"

-- BUTTON library
require "libs.simple-slider"
BUTTON = require "libs.simple-button"
BUTTON.setDefaultOption{
    draw = function(self)
        local need_big_font = (self.font == FONT_big)

        love.graphics.setColor(self.backgroundColor)
        love.graphics.rectangle('fill', self.x, self.y, self.w, self.h, self.r)

        if self._pressed then
            love.graphics.setColor(self.pressColor)
            love.graphics.rectangle('fill', self.x, self.y, self.w, self.h, self.r)
        elseif self._hovering then
            love.graphics.setColor(self.hoverColor)
            love.graphics.rectangle('fill', self.x, self.y, self.w, self.h, self.r)
        end

        local text = type(self.text) == 'function' and self.text() or self.text

        local lineAmount
        do
            local _, t = self.font:getWrap(text, (self.w - 5) * 2)
            lineAmount = #t
        end

        local _font_height = need_big_font and font_big_height or font_height

        local textHeight = _font_height * (lineAmount * 0.5)
        local textPos = self.y + (self.h * 0.5) - textHeight

        if self.font == FONT_big then
            drawBigText(text, self.x + 2.5, textPos, self.w - 5, self.textOrientation, self.textColor)
        else
            drawText(text, self.x + 2.5, textPos, self.w - 5, self.textOrientation, self.textColor)
        end

        love.graphics.setColor(self.borderColor)
        love.graphics.setLineWidth(1)
        love.graphics.rectangle('line', self.x, self.y, self.w, self.h, self.r)
    end,
    backgroundColor = {0, 0, 0, 0.8},
    pressColor = {0.4, 1, 1, 0.5},
    borderColor = {1, 1, 1, 0.8},
}

-- Graphics
local gc_newImage, gc_newVideo = love.graphics.newImage, love.graphics.newVideo
BACKGROUNDS = {}
LILY.loadMulti{
    {'newVideo', "res/backgrounds/green_waterfall.ogv", {audio=false}},
    {'newVideo', "res/backgrounds/water.ogv", {audio=false}},
    {'newVideo', "res/backgrounds/green_streams.ogv", {audio=false}},
    {'newVideo', "res/backgrounds/streams.ogv", {audio=false}},
    {'newVideo', "res/backgrounds/red_forest_waterfall.ogv", {audio=false}},
    {'newVideo', "res/backgrounds/flowers_rain.ogv", {audio=false}},
    {'newVideo', "res/backgrounds/moonlight_tree.ogv", {audio=false}},
    {'newVideo', "res/backgrounds/lisa_frank.ogv", {audio=false}},
    {'newVideo', "res/backgrounds/snowy_trees.ogv", {audio=false}},
    {'newVideo', "res/backgrounds/snowy_cabin.ogv", {audio=false}},
}:onComplete(
    function(_, lilies)
        for i = 0, 9 do BACKGROUNDS[i] = lilies[i + 1][1] end
        LOADED_BACKGROUND = true;
    end)

BLOCKS = {
    ["2tie"] = {
        R   = gc_newImage("res/img/r.png"),
        O   = gc_newImage("res/img/o.png"),
        Y   = gc_newImage("res/img/y.png"),
        G   = gc_newImage("res/img/g.png"),
        C   = gc_newImage("res/img/b.png"),
        B   = gc_newImage("res/img/i.png"),
        M   = gc_newImage("res/img/v.png"),
        F   = gc_newImage("res/img/bl.png"),
        A   = gc_newImage("res/img/bl.png"),
        X   = gc_newImage("res/img/t.png"),
        W   = gc_newImage("res/img/w.png"),
        R_d = gc_newImage("res/img/r_d.png"),
        O_d = gc_newImage("res/img/o_d.png"),
        Y_d = gc_newImage("res/img/y_d.png"),
        G_d = gc_newImage("res/img/g_d.png"),
        C_d = gc_newImage("res/img/b_d.png"),
        B_d = gc_newImage("res/img/i_d.png"),
        M_d = gc_newImage("res/img/v_d.png"),
    }
}

COLOUR_SCHEMES = {
    Arika = {
        I = "R",
        L = "O",
        J = "B",
        S = "M",
        Z = "G",
        O = "Y",
        T = "C",
    }
}

-- BGMs and SFXs
local audio_newSource = love.audio.newSource
SOUNDS = {}
LILY.loadMulti{
    {"newSource", "res/se/bottom.wav", "static"},
    {"newSource", "res/se/lock.wav", "static"},
    {"newSource", "res/se/erase.wav", "static"},
    {"newSource", "res/se/fall.wav", "static"},
    {"newSource", "res/se/ready.wav", "static"},
    {"newSource", "res/se/promote.wav", "static"},
    {"newSource", "res/se/demote.wav", "static"},
    {"newSource", "res/se/autopromote.wav", "static"},
    {"newSource", "res/bgm/firsthalf.flac", "static"},
    {"newSource", "res/bgm/secondhalf.flac", "static"},
    {"newSource", "res/bgm/title.flac", "static"},
}:onComplete(
    function(_, lilies)
        SOUNDS.bottom, SOUNDS.lock, SOUNDS.erase, SOUNDS.fall = unpack(lilies, 1, 4)
        SOUNDS.ready, SOUNDS.promote, SOUNDS.demote, SOUNDS.autopromote = unpack(lilies, 5, 8)
        SOUNDS.bgm_firsthalf, SOUNDS.bgm_secondhalf, SOUNDS.bgm_title = unpack(lilies, 9)

        for k, v in pairs(SOUNDS) do SOUNDS[k] = v[1] end -- Again, to avoid userdata
        LOADED_MUSIC = true;
    end
)

function PlaySE(sound, subsound)
    if sound ~= nil then
        if subsound ~= nil then
            SOUNDS[sound][subsound]:setVolume(0.4)
            if SOUNDS[sound][subsound]:isPlaying() then
                SOUNDS[sound][subsound]:stop()
            end
            SOUNDS[sound][subsound]:play()
        else
            SOUNDS[sound]:setVolume(0.4)
            if SOUNDS[sound]:isPlaying() then
                SOUNDS[sound]:stop()
            end
            SOUNDS[sound]:play()
        end
    end
end

function PlaySEOnce(sound, subsound)
    if sound ~= nil then
        if subsound ~= nil then
            SOUNDS[sound][subsound]:setVolume(0.4)
            if SOUNDS[sound][subsound]:isPlaying() then
                return
            end
            SOUNDS[sound][subsound]:play()
        else
            SOUNDS[sound]:setVolume(0.4)
            if SOUNDS[sound]:isPlaying() then
                return
            end
            SOUNDS[sound]:play()
        end
    end
end
