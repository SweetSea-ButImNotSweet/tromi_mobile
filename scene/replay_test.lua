local ReplayTestScene = SCENE:extend()

local GAME
local error_message
local valid_data
local prev_scene

function ReplayTestScene:new(input_file)
    prev_scene = SCENE
    
    if not input_file then
        valid_data = false
        return
    else
        valid_data = true
    end
    self.input_file = input_file

    GAME = require("game.gamemode")
    local okay, err = pcall(function()
        GAME:new("TRO", input_file, "19k")
        GAME:initialize(require"game.rotation")
    end)
    if not okay then
        -- TODO
        error_message = err
        love.filesystem.remove(REPLAY_DIR..self.input_file)
    end
end

function ReplayTestScene:render()
    prev_scene:render()
    love.graphics.setColor(0, 0, 0, 0.8)
    love.graphics.rectangle("fill", 0, 0, 640, 480)

    if valid_data then
        if error_message then
            drawText("Replay test failed!", 80, 40, 1000)
            drawText("Press any key to go back. Anyway here is the error info:", 80, 70, 1000)
            drawText(error_message, 80, 100, 560)
        else
            drawText("Replay test finished!", 80, 40, 1000)
            drawText("Your replay was imported. Press any key to go back.", 80, 70, 1000)
        end
    else
        drawText("Replay test failed! Not Tromi's replay data", 80, 40, 1000)
        drawText("Press any key to go back, and check your device's clipboard again!", 80, 70, 1000)
    end
end


function ReplayTestScene:onInputPress()
    SCENE = ReplaySelectScene()
end

return ReplayTestScene