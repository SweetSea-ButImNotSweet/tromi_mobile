local TrainingScene = SCENE:extend()
TrainingScene.title = "Max Gravity Training"

local menuKey = BUTTON.new{
    text = CHAR.icon.menu.." MENU",
    x = 10, y = 10, w = 70, h = 30,
    codeWhenReleased = function() SCENE = TitleScene() end
}

function TrainingScene:new()
    BUTTON.reset{menuKey}
    VCTRL[9].show = true

    game_mode = require 'game.gamemode'
    if PENTO_MODE then
        ruleset = require 'game.rotation_pent'
    else
        ruleset = require 'game.rotation'
    end
    self.retry_mode = game_mode
    self.retry_ruleset = ruleset
    -- self.secret_inputs = inputs
    self.game = game_mode()
    self.ruleset = ruleset(self.game)
    self.game:initialize(self.ruleset)
    self.inputs = {
        left=false,
        right=false,
        up=false,
        down=false,
        rotate_left=false,
        rotate_left2=false,
        rotate_right=false,
        rotate_right2=false,
    }
    self.paused = false
end

function TrainingScene:update()
    local inputs = {}
    for input, value in pairs(self.inputs) do
        inputs[input] = value
    end
    self.game:update(inputs, self.ruleset)
    self.game.grid:update()
end

function TrainingScene:render()
    self.game:draw(self.paused)
    menuKey:draw()
    VCTRL.draw()
end

function TrainingScene:onInputPress(e)
    if e.type == "mouse" or (e.type == "touch" and not VCTRL.press(e.x, e.y, e.id)) then
        menuKey:press(e.x, e.y, e.id)
    elseif (self.game.game_over or self.game.completed) and (e.input == "menu_back" or e.input == "rotate_right") and self.game.game_over_frames > 50 then
        SCENE = TitleScene()
    elseif (e.input == "menu_back") then
        SCENE = TitleScene()
    elseif e.input == "restart" or e.input == "menu_decide" then
        SCENE = TrainingScene()
    elseif e.input and string.sub(e.input, 1, 5) ~= "menu_" then
        self.inputs[e.input] = true
    end
end

function TrainingScene:onInputRelease(e)
    if e.type == "mouse" or (e.type == "touch" and not VCTRL.release(e.id)) then
        menuKey:release(e.x, e.y, e.id)
    elseif e.input and string.sub(e.input, 1, 5) ~= "menu_" then
        self.inputs[e.input] = false
    end
end

function TrainingScene:onInputMove(e)
    menuKey._hovering = menuKey:isHovering(e.x, e.y)
end

return TrainingScene
