local DataManagementScene = SCENE:extend()
DataManagementScene.title = "Data Management"

local data_title = {
    "Open save folder",
    "Manage accounts (WIP)",
    "Erase all high scores",
    "RESET ALL!",
    "Back"
}
local data_icon = {
    CHAR.key .windows,
    CHAR.icon.menu,
    CHAR.icon.erase,
    CHAR.icon.erase,
    CHAR.icon.home,
}
local data_explaination = {
    "(Only for PC)\nOpen save folder in a new window",
    "You can view and delete your accounts here.",
    "Erase all your high scores\nThis will make high score table go back to default",
    "Erase all accounts, replays and all high scores!",
    "Back to main menu"
}

local NULL = function() end
local settings_func = {
    function() love.system.openURL(love.filesystem.getSaveDirectory()) end,
    function() return UserManagementScene() end,
    function() return EraseHighScoresScene() end,
    function() return ResetAllScene() end,
    function() return TitleScene() end,
}

function DataManagementScene:new()
    self.data_menu_state = 1
end

function DataManagementScene:changeOption(rel)
    local len = #data_title
    self.data_menu_state = (self.data_menu_state + len + rel - 1) % len + 1
end

function DataManagementScene:render()
    MainBackground()

    love.graphics.setColor(0, 0, 0, 0.7)
    love.graphics.rectangle("fill", 30,  60, 580,  85, 10, 10)  -- Tromi
    love.graphics.rectangle("fill", 30, 165, 580, 225, 10, 10)  -- Menu
    drawBigText(CHAR.icon.export.." DATA MANAGEMENT", 40, 85, 400, "left")

    -- Selecting
    love.graphics.setColor(0.4, 1, 1, 0.5)
    love.graphics.rectangle("fill", 40, 135 + 40 * self.data_menu_state, 270, 40)
    -- Text
    for i = 1, #data_title do
        drawText   (data_title[i], 85, 145 + 40 * i, 230, "left")
        drawBigText(data_icon [i], 45, 135 + 40 * i,  50, "left")
    end

    drawText(data_explaination[self.data_menu_state], 330, 175, 610 - 15 - 330, "left")
end

---@param e SCENE_onInput
function DataManagementScene:onInputPress(e)
    if e.input == "menu_back" or e.scancode == "escape" then SCENE = TitleScene()
    elseif e.input == "menu_decide" or e.key == "return" then
        local s = settings_func[self.data_menu_state]()
        if s then SCENE = s end
    elseif e.input == "down" or e.scancode == "down" then
        self:changeOption(1)
    elseif e.input == "up" or e.scancode == "up" then
        self:changeOption(-1)

    elseif e.type == "touch" or e.type == "mouse" then
        local x, y = e.x, e.y
        local testOption = function(sel)
            if sel ~= self.data_menu_state then
                self.data_menu_state = sel
            else
                self:onInputPress{input = "menu_decide"}
            end
        end
        if (
            x >=  40 and x <= 310 and
            y >= 175 and y <= 375
        ) then
            local sel = math.floor((y - 175) / 40) + 1
            if sel <= #settings_func then
                testOption(sel)
            end
        end
    end
end

return DataManagementScene
