---@class SCENE
local Title2Scene = SCENE:extend()
Title2Scene.title = "Title"

local input_code = {}

local main_menu_title = {
    "New game"              ,
    "Replay"                ,
    "Data management"       ,
    "Mobile port repository",
    "About"                 ,

    "Max gravity Training",
    "Leaderboard"         ,
    "Settings"            ,
    "Official homepage"   ,
    "Exit game"           ,
}
local half_pos = math.roundUnit(#main_menu_title / 2)

local main_menu_scenes = {
    NameEntryScene,
    ReplaySelectScene,
    DataManagementScene,
    function() love.system.openURL("https://gitea.com/SweetSea-ButImNotSweet/tromi_mobile") end,
    AboutScene,

    TrainingScene,
    function() love.system.openURL("https://mycophobia.org/forums/viewtopic.php?t=29") end,
    SettingsScene,
    function() love.system.openURL("https://mycophobia.org/tromi") end,
    ExitScene
}

local main_menu_icons = {
    CHAR.icon.play,
    CHAR.icon.rewind,
    CHAR.icon.export,
    CHAR.icon.home,
    CHAR.icon.info,

    CHAR.icon.toDown,
    CHAR.icon.globe,
    CHAR.icon.settings,
    CHAR.icon.home,
    CHAR.icon.back,
}

function Title2Scene:new()
    if SOUNDS['bgm_firsthalf']:isPlaying() or SOUNDS['bgm_secondhalf']:isPlaying() or not SETTINGS["music"] then
        love.audio.stop()
    end
    self.main_menu_state = 1
    PENTO_MODE = false
    input_code = {0,0,0,0,0,0,0,0}
end

function Title2Scene:changeOption(rel)
    local len = #main_menu_title
    self.main_menu_state = (self.main_menu_state + len + rel - 1) % len + 1
end

function Title2Scene:render()
    MainBackground()

    love.graphics.setColor(0, 0, 0, 0.7)
    love.graphics.rectangle("fill", 30,  60, 580,  85, 10, 10)  -- Tromi
    love.graphics.rectangle("fill", 30, 165, 580, 225, 10, 10)  -- Menu

    drawBigText("Tromi", 40, 65, 100, "left")
    drawText("Mobile 1.4 - PC 2.3", 150, 78, 200, "left")
    drawText("https://mycophobia.org\nhttps://github.com/SweetSea-ButImNotSweet/", 40, 100, 300, "left")

    if PENTO_MODE then
        drawBigText("PENTO MODE", 400, 85, 200, "right")
    end

    love.graphics.setColor(1, 1, 1, 0.5)
    love.graphics.setLineWidth(1)
    love.graphics.line(320, 175, 320, 375)

    -- Selecting
    love.graphics.setColor(0.4, 1, 1, 0.5)
    if self.main_menu_state > half_pos then
        love.graphics.rectangle("fill", 330, 135 + 40 * (self.main_menu_state - half_pos), 270, 40)
    else
        love.graphics.rectangle("fill",  40, 135 + 40 *  self.main_menu_state, 270, 40)
    end
    -- Text
    for i = 1, half_pos do
        drawText(main_menu_title[i]      ,  45, 145 + 40 * i, 230, "right")
        drawText(main_menu_icons[i] or '', 285, 145 + 40 * i,  20, "center")
    end
    for i = half_pos + 1, #main_menu_title do
        drawText(main_menu_title[i]      , 365, 145 + 40 * (i - half_pos), 230, "left")
        drawText(main_menu_icons[i] or '', 335, 145 + 40 * (i - half_pos),  20, "center")
    end
end

local function checkCode(c)
    if not PENTO_MODE then
        table.insert(input_code, c)
        if #input_code > 8 then
            table.remove(input_code, 1)
        end

        local code_string = table.concat(input_code, ',')
        if (
            code_string == "-1,-1,-1,-1,1,1,1,1" or
            code_string == "2,2,2,2,2,2,2,2"
        ) then
            PENTO_MODE = true
        end
    end
end

---@param e SCENE_onInput
function Title2Scene:onInputPress(e)
    if e.input == "menu_back" or e.scancode == "escape" then SCENE = TitleScene()
    elseif e.input == "menu_decide" or e.key == "return" then
        local s = main_menu_scenes[self.main_menu_state]()
        if s then SCENE = s end
    elseif e.input == "down" or e.scancode == "down" then
        self:changeOption(1)
    elseif e.input == "up" or e.scancode == "up" then
        self:changeOption(-1)
    elseif e.input == "left" or e.scancode == "left" then
        self:changeOption(-half_pos)
        checkCode(-1)
    elseif e.input == "right" or e.scancode == "right" then
        self:changeOption(half_pos)
        checkCode(1)

    elseif e.type == "touch" or e.type == "mouse" then
        local x, y = e.x, e.y
        local testOption = function(sel)
            if sel ~= self.main_menu_state then
                self.main_menu_state = sel
            else
                self:onInputPress{input = "menu_decide"}
            end
        end
        -- 40 175 320 375       320 175 600 375
        if y >= 175 and y <= 375 then
            local sel = math.floor((y - 175) / 40) + 1
            if x >= 40 and x <= 310 and sel <= half_pos then
                testOption(sel)
            elseif (
                x >= 330 and x <= 600 and
                sel >= 1 and sel + half_pos <= #main_menu_scenes
            ) then
                testOption(sel + half_pos)
            end
        elseif (
            x >= 460 and y >= 85 and
            x <= 600 and y <= 125
        ) then
            checkCode(2)
        end
    end
end

return Title2Scene
