local TouchConfigPreviewScene = SCENE:extend()
TouchConfigPreviewScene.title = "Touchscreen config (Preview)"

local Grid = require 'game.grid'
local GameMode = require 'game.gamemode'

local buttonList
buttonList = {
    previewToggle = BUTTON.new{
        text = "Preview\nOFF",
        x = 570, y = 60, w = 60, h = 40,
        codeWhenReleased = function()
            VCTRL.release()
            BUTTON.release(buttonList)
            SCENE = TouchConfigScene()
        end
    },
}
local secret_grade_grid = {}
do
    local colour_names = {'R', 'O', 'Y', 'G', 'C', 'B', 'M'}
    local color_blocks = {E = {colour =  "", flash = 0, skin = "2tie"}}
    for _, v in pairs(colour_names) do
        color_blocks[v] = {colour = v, flash = 0, skin = "2tie"}
    end

    for y = 20, 1, -1 do
        local hole_pos = (y > 10 and 21 - y) or (y > 1 and y - 1) or 2
        local current_colour_name = colour_names[(20 - y)%7+1]
        local row = {}
        for x = 1, 10 do
            if x == hole_pos then
                row[x] = color_blocks.E
            else
                row[x] = color_blocks[current_colour_name]
            end
        end
        secret_grade_grid[y] = row
    end
end

function TouchConfigPreviewScene:new()
    VCTRL.toggle(true)

    VCTRL.focus = nil
    Grid:new(10, 20)
    Grid.grid = secret_grade_grid
    -- TODO
end
function TouchConfigPreviewScene:update()
    BUTTON.update(buttonList)
end

function TouchConfigPreviewScene:render()
    if not BACKGROUNDS[0]:isPlaying() then
        BACKGROUNDS[0]:play()
    end
    if BACKGROUNDS[0]:tell() >= 0.5 then
        BACKGROUNDS[0]:rewind()
    end
    love.graphics.setColor(0.7, 0.7, 0.7)
    love.graphics.draw(BACKGROUNDS[0])

    GameMode.drawFrame{grid = {width = 10, height = 20}}
    Grid:draw(false, 1)

    BUTTON.draw(buttonList)
    VCTRL.draw()
end

---@param e SCENE_onInput
function TouchConfigPreviewScene:onInputMove(e)
    if e.type == "mouse" then
        BUTTON.checkHovering(buttonList, e.x, e.y)
    end
end
---@param e SCENE_onInput
function TouchConfigPreviewScene:onInputPress(e)
    if e.type ~= "virtual" and e.input == 'menu_back' then SCENE = InputConfigScene() end
    if e.type == "mouse" or e.type == "touch" then
        if not BUTTON.press(buttonList, e.x, e.y, e.id) then
            VCTRL.press(e.x, e.y, e.id or 1)
        end
    end
end
---@param e SCENE_onInput
function TouchConfigPreviewScene:onInputRelease(e)
    if e.type == "mouse" or e.type == "touch" then
        if not BUTTON.release(buttonList, e.x, e.y, e.id) then
            VCTRL.release(e.id or 1)
        end
    end
end

return TouchConfigPreviewScene