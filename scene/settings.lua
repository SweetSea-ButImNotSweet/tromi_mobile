local SettingsScene = SCENE:extend()
SettingsScene.title = "Settings"

local settings_title = {
    "Music",
    "Lines",
    "Fullscreen (F4)",
    "Input configuration (F2)",
    "Back",
}
local settings_explaination = {
    "Enable music?\nThis does not apply to sound effects.",
    "Show level and lines counter?\nThis setting is ignored when replaying.",
    "Enter or leave fullscreen\nYou can press F4 key at any screen to do this quick.",
    "This is where you can re-configure your keybinds.\nYou can press F2 on the keyboard to open this quick\n\nTip for TV:\n\t88663366: enable TV mode.\n\t11111111 disable TV mode",
    "Back to main menu"
}

local settings_func = {
    function()
        SETTINGS["music"] = not SETTINGS["music"]
        love.audio.stop()
    end,
    function() SETTINGS["lines"] = not SETTINGS["lines"] end,
    function() love.keypressed("f4", "f4") end,
    InputConfigScene,
    function() return TitleScene() end,
}

function SettingsScene:new()
    self.settings_menu_state = 1
end

function SettingsScene:changeOption(rel)
    local len = #settings_title
    self.settings_menu_state = (self.settings_menu_state + len + rel - 1) % len + 1
end

function SettingsScene:render()
    MainBackground()

    love.graphics.setColor(0, 0, 0, 0.7)
    love.graphics.rectangle("fill", 30,  60, 580,  85, 10, 10)  -- Tromi
    love.graphics.rectangle("fill", 30, 165, 580, 225, 10, 10)  -- Menu
    drawBigText(CHAR.icon.settings.." SETTINGS", 40, 85, 200, "left")

    -- Selecting
    love.graphics.setColor(0.4, 1, 1, 0.5)
    love.graphics.rectangle("fill",  40, 135 + 40 * self.settings_menu_state, 270, 40)
    -- Text
    for i = 1, #settings_title do
        drawText(settings_title[i], 45, 145 + 40 * i, 230, "left")
    end
    drawBigText(SETTINGS["music"] and CHAR.icon.checkMark or CHAR.icon.crossMark, 260, 175, 50, "center")
    drawBigText(SETTINGS["lines"] and CHAR.icon.checkMark or CHAR.icon.crossMark, 260, 215, 50, "center")
    drawBigText(love.window.getFullscreen() and CHAR.icon.checkMark or CHAR.icon.crossMark, 260, 255, 50, "center")
    drawBigText(CHAR.key.keyboard , 260, 295, 50, "center")
    drawBigText(CHAR.icon.home    , 260, 335, 50, "center")

    drawText(settings_explaination[self.settings_menu_state], 330, 175, 610 - 15 - 330, "left")
end

---@param e SCENE_onInput
function SettingsScene:onInputPress(e)
    if e.input == "menu_back" or e.scancode == "escape" then SCENE = TitleScene()
    elseif e.input == "menu_decide" or e.key == "return" then
        local s = settings_func[self.settings_menu_state]()
        if s then SCENE = s end
    elseif e.input == "down" or e.scancode == "down" then
        self:changeOption(1)
    elseif e.input == "up" or e.scancode == "up" then
        self:changeOption(-1)

    elseif e.type == "touch" or e.type == "mouse" then
        local x, y = e.x, e.y
        local testOption = function(sel)
            if sel ~= self.settings_menu_state then
                self.settings_menu_state = sel
            else
                self:onInputPress{input = "menu_decide"}
            end
        end
        if (
            x >=  40 and x <= 310 and
            y >= 175 and y <= 375
        ) then
            local sel = math.floor((y - 175) / 40) + 1
            if sel <= #settings_func then
                testOption(sel)
            end
        end
    end
end

return SettingsScene
