local LinesToggleScene = SCENE:extend()
LinesToggleScene.title = "Show lines during game:"

function LinesToggleScene:update()
    SETTINGS["lines"] = not SETTINGS["lines"]
    SCENE = TitleScene()
end

return LinesToggleScene