local NameEntryScene = SCENE:extend()
NameEntryScene.title = "Game Start"

local buttonList = {
    BUTTON.new{
        text = "↑\nCHAR", font = FONT_big,
        x = 25, y = 120, w = 80, h = 80,
        codeWhenPressed  = function() SCENE:onInputPress  {input = "left"} end,
        codeWhenReleased = function() SCENE:onInputRelease{input = "left"} end,
    },
    BUTTON.new{
        text = "↓\nCHAR", font = FONT_big,
        x = 115, y = 120, w = 80, h = 80,
        codeWhenPressed  = function() SCENE:onInputPress  {input = "right"} end,
        codeWhenReleased = function() SCENE:onInputRelease{input = "right"} end,
    },
    BUTTON.new{
        text = "←\nESC", font = FONT_big,
        x = 25, y = 210, w = 80, h = 80,
        codeWhenPressed  = function() SCENE:onInputPress  {input = "menu_back"} end,
        codeWhenReleased = function() SCENE:onInputRelease{input = "menu_back"} end,
    },
    BUTTON.new{
        text = "→\nENTER", font = FONT_big,
        x = 115, y = 210, w = 80, h = 80,
        codeWhenPressed  = function() SCENE:onInputPress  {input = "menu_decide"} end,
        codeWhenReleased = function() SCENE:onInputRelease{input = "menu_decide"} end,
    },
    BUTTON.new{
        text = CHAR.key.keyboard.." Open OSK", font = FONT_big,
        x = 25, y = 300, w = 170, h = 40,
        codeWhenReleased = function() love.keyboard.setTextInput(true, 215, 175, 160, 160) end
    }
}

local Grid = require 'game.grid'

function NameEntryScene:new()
    VCTRL.toggle(false)

    self.chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890."
    self.char_pos = 1
    self.name_entry = {'A','A','A'}
    self.entry_pos = 1
    self.entry_chars = table.concat(self.name_entry, '', 1, 3)
    self.grid = Grid(10, 20)
    self.repeat_limit = 10
    self.repeat_counter = self.repeat_limit-1
    self.direction = nil
    self.grade = 0
    self.wins = 0
    self.plays = 0
    self.gradeNames = {
    "19k", "18k", "17k", "16k", "15k", "14k", "13k", "12k", "11k",
    "10k", "9k", "8k", "7k", "6k", "5k", "4k", "3k", "2k", "1k",
    "1D", "2D", "3D", "4D", "5D", "6D", "7D", "8D", "9D"
    }
    if SETTINGS['last_entry'] ~= nil then
        self.name_entry = {SETTINGS['last_entry']:sub(1,1),SETTINGS['last_entry']:sub(2,2),SETTINGS['last_entry']:sub(3,3)}
        self.entry_pos = 3
    end
    if love.filesystem.getInfo(HIscoreFILE) then
        self.hi_scores = FILE.read(HIscoreFILE)
    else
        self.hi_scores = {"TRO",0,"MIT",0,"ROM",0,"ITR",0,"OMI",0}
    end
end
function NameEntryScene:drawGradeList(left, top)
	love.graphics.setColor(0,0,0,0.5)
	love.graphics.rectangle("fill", left+3, top+3, 200, 240, 10, 10)
	love.graphics.setColor(0.05,0.05,0.05,1)
	love.graphics.rectangle("fill", left, top, 200, 240, 10, 10)
	drawText("Grade list:", left+15, top+10, 1000, "left")
    drawText("Beginner\n19 kyu\n18 kyu\n17 kyu\n16 kyu\n15 kyu\n14 kyu\n13 kyu\n12 kyu\n11 kyu\n10 kyu", left+15, top+25, 1000, "left")
    drawText("Intermed.\n9 kyu\n8 kyu\n7 kyu\n6 kyu\n5 kyu\n4 kyu\n3 kyu\n2 kyu\n1 kyu", left+80, top+25, 1000, "left")
    drawText("Expert\n1 Dan\n2 Dan\n3 Dan\n4 Dan\n5 Dan\n6 Dan\n7 Dan\n8 Dan\n9 Dan", left+145, top+25, 1000, "left")
end

function NameEntryScene:render()
    MainBackground()
    BUTTON.draw(buttonList)

	love.graphics.setColor(1, 1, 1, 1)
	love.graphics.line(216,80,216,80+(16*self.grid.height))
	love.graphics.line(216+(16*self.grid.width),80,216+(16*self.grid.width),80+(16*self.grid.height))
	love.graphics.line(216,80+(16*self.grid.height),216+(16*self.grid.width),80+(16*self.grid.height))
	love.graphics.line(216,80,216+(16*self.grid.width),80)
    love.graphics.setColor(0, 0, 0, 1)
    love.graphics.rectangle(
        "fill", 216, 80,
        16 * self.grid.width, 16 * self.grid.height
    )
    love.graphics.setColor(1, 1, 1, 1)
    drawText('Enter your initials:', 227, 180, 200, "left")
    drawBigText(self.entry_chars, 272, 200, 200, "left")
    drawText('o', 262+(self.entry_pos*14), 225, 200, "left")
    self:drawGradeList(397, 40)
    love.graphics.setColor(0,0,0,0.5)
    love.graphics.rectangle("fill", 400, 295, 130, 130, 10, 10)
    love.graphics.setColor(0.05,0.05,0.05,1)
    love.graphics.rectangle("fill", 397, 292, 130, 130, 10, 10)
    drawText("Best scores:", 410, 297, 1000, "left")
    for i = 2, 10, 2 do
        drawText(self.hi_scores[i-1]..' - '..self.hi_scores[i], 410, 297+(i*10), 1000, "left")
    end
    if self.entry_pos == 4 then
        drawText('Press confirm\nto play', 255, 290, 1000)
    end
    if self.grade > 0 then
		drawText(string.format('Games: %s', self.plays), 255, 250, 1000)
		drawText(string.format('Grade: %s', self.gradeNames[self.grade]), 255, 270, 1000)
    end
end

function NameEntryScene:update()
    if self.direction == "left" then
        if self.repeat_counter >= self.repeat_limit then
            self.char_pos = self.char_pos - 1
            if self.char_pos < 1 then self.char_pos = 37 end
            self.name_entry[self.entry_pos] = self.chars:sub(self.char_pos, self.char_pos)
            self.repeat_counter = 0
        end
        self.repeat_counter = self.repeat_counter + 1
    elseif self.direction == "right" then
        if self.repeat_counter >= self.repeat_limit then
            self.char_pos = self.char_pos + 1
            if self.char_pos > 37 then self.char_pos = 1 end
            self.name_entry[self.entry_pos] = self.chars:sub(self.char_pos, self.char_pos)
            self.repeat_counter = 0
        end
        self.repeat_counter = self.repeat_counter + 1
    end
    self.entry_chars = table.concat(self.name_entry, '', 1, 3)
end

function NameEntryScene:onInputMove(e)
    if e.type == "mouse" then
        BUTTON.checkHovering(buttonList, e.x, e.y)
    end
end

function NameEntryScene:getPlayInfo(player_name)
    if love.filesystem.getInfo((SAVE_DIR..player_name.."_grade_history.sav")) then
        grade_history = FILE.read(SAVE_DIR..player_name.."_grade_history.sav")
        self.grade = grade_history[1]
        self.wins = grade_history[2]
        self.plays = grade_history[4]
    else
        self.grade, self.wins, self.plays = 0, 0, 0
    end
end
function NameEntryScene:onInputPress(e)
    local name = string.lower(table.concat(self.name_entry, '', 1, 3))
    if e.type == "mouse" or e.type == "touch" then
        BUTTON.press(buttonList, e.x, e.y, e.id)
    elseif e.input == "menu_decide" or e.input == "rotate_left" or e.scancode == "return" then
        if self.entry_pos == 4 then
            BUTTON.release(buttonList, e.x, e.y, e.id)
            SETTINGS['last_entry'] = name:upper()
            SCENE = GameScene(name:lower())
        elseif self.entry_pos == 3 then self:getPlayInfo(name)
        else
            self.name_entry[self.entry_pos  ] = self.chars:sub(self.char_pos, self.char_pos)
            self.name_entry[self.entry_pos+1] = self.chars:sub(self.char_pos, self.char_pos)
        end
        self.entry_pos = self.entry_pos + 1
    elseif e.input == "left" or e.scancode == "left" then
        self.direction = "left"
    elseif e.input == "right" or e.scancode == "right" then
        self.direction = "right"
    elseif e.input == "menu_back" or e.input == "rotate_right" or e.scancode == "delete" or e.scancode == "backspace" then
        if self.entry_pos == 1 then
            BUTTON.release(buttonList, true)
            SCENE = TitleScene()
        else
            self.name_entry[self.entry_pos] = 'A'
            self.name_entry[self.entry_pos-1] = 'A'
            self.char_pos = 1
            self.entry_pos = self.entry_pos - 1
            self.grade = 0
        end
    elseif e.key and #e.key == 1 then
        local pos = string.find(self.chars, string.upper(e.key), 1, true)
        if pos then
            if self.entry_pos <= 3 then
                self.char_pos = pos
                self.name_entry[self.entry_pos]     = string.upper(e.key)
                self.name_entry[self.entry_pos + 1] = string.upper(e.key)
            end
            self.entry_pos = math.min(self.entry_pos + 1, 4)
            if self.entry_pos == 4 then self:getPlayInfo(string.lower(table.concat(self.name_entry, '', 1, 3))) end
        end
    end
end

function NameEntryScene:onInputRelease(e)
    if e.type == "mouse" or e.type == "touch" then
        BUTTON.release(buttonList, e.x, e.y, e.id)
    elseif (
        MOBILE and not SETTINGS.tvMode and
        self.entry_pos == 4
    ) then love.keyboard.setTextInput(false)
    elseif e.input == "left" or e.scancode == "left" or e.input == "right" or e.scancode == "right" then
        self.direction = nil
        self.repeat_counter = self.repeat_limit-1
    end
end

return NameEntryScene
