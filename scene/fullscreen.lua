local FullscreenScene = SCENE:extend()
FullscreenScene.title = "Fullscreen"

function FullscreenScene:new()
end

function FullscreenScene:update()
    SETTINGS["fullscreen"] = not SETTINGS["fullscreen"]
    love.window.setFullscreen(SETTINGS["fullscreen"])
    SCENE = TitleScene()
end

function FullscreenScene:render()
end

function FullscreenScene:changeOption(rel)
end

function FullscreenScene:onInputPress(e)
end

return FullscreenScene
