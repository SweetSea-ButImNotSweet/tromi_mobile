
---@class SCENE
local AboutScene = SCENE:extend()
AboutScene.title = "About"

local current_page = 1
local maxPage = 2
local buttonList

local function changePage(rel)
    current_page = (current_page + maxPage + rel - 1) % maxPage + 1
end

buttonList = {
    BUTTON.new{
        text = function()
            if current_page < maxPage then
                return ("%s Next (%s/%s)"):format(CHAR.key.right, current_page, maxPage)
            else
                return ("%s Back (%s/%s)"):format(CHAR.icon.toLeft, current_page, maxPage)
            end
        end,
        textOrientation = "center",
        x = 185, y = 440, w = 130, h = 30,
        codeWhenReleased = function()
            changePage(1)
            BUTTON.reset(buttonList)
        end
    },
    BUTTON.new{
        text = CHAR.icon.home.." Title",
        textOrientation = "center",
        x = 325, y = 440, w = 130, h = 30,
        codeWhenReleased = function() SCENE = TitleScene() end
    }
}

function AboutScene:new()
    current_page = 1
    BUTTON.reset(buttonList)
end

function AboutScene:render()
    MainBackground()
    love.graphics.setColor(0, 0, 0, 0.7)
    love.graphics.rectangle("fill", 0, 0, 640, 480)

    BUTTON.draw(buttonList)

    if current_page == 1 then
        -- Design & programming - Mycophobia
        drawText   ("Design & programming"                             ,  20,  10, 180, "center")
        drawBigText("mycophobia"                                       ,  20,  25, 180, "center")
        drawText   ("https://mycophobia.org"                           ,  20,  60, 180, "center")
        -- Cambridge - Based on
        drawText   ("Based on"                                         , 230,  10, 180, "center")
        drawBigText("Cambridge"                                        , 230,  25, 180, "center")
        drawText   ("Cambridge contributors\nhttps://t-sp.in/cambridge", 230,  60, 180, "center")
        -- Port to TV & mobile - SweetSea
        drawText   ("Port to TV & mobile"                              , 450,  10, 180, "center")
        drawBigText("SweetSea"                                         , 450,  25, 180, "center")
        drawText   ("https://github.com/\nSweetSea-ButImNotSweet"      , 450,  60, 180, "center")
        -- Design & programming - Mycophobia
        drawText   ("RNG Consultant"                                   ,  20, 120, 180, "center")
        drawBigText("colour_thief"                                     ,  20, 135, 180, "center")
        -- Design & programming - Mycophobia
        drawText   ("Launcher for Mac"                                 , 450, 120, 180, "center")
        drawBigText("nightmareci"                                      , 450, 135, 180, "center")
        drawText   ("http://iblock.red"                                , 450, 170, 180, "center")

        local other_text_y = 200
        --- Other credit text, small text's y on the right = big text's y + 13, on the left: big text's y + 30
        -- Music - Jerry Martin
        drawBigText("Music", 0, other_text_y, 250, "right")
        drawText("all rights reserved", 0, other_text_y + 30, 250, "right")
        drawText("Jerry Martin - https://jerrymartinmusic.com \nJuraj Stanik - https://www.jurajstanik.com", 300, other_text_y + 13, 400, "left")
        -- Background - PixaBays
        drawBigText("Background", 0, other_text_y + 60, 250, "right")
        drawText("by people on PixaBays", 0, other_text_y + 90, 250, "right")
        drawText(
            "Joe_hackney    VisualSkyFX\n"..
            "yokim    Favorisxp    Any_Ann",
            300, other_text_y + 73, 400, "left"
        )
        -- Special thanks
        drawBigText("Testing and/or\nCool Features", 0, other_text_y + 120, 250, "right")
        drawText(
            "netdoll    esquatre    Kirby703\n"..
            "switchpalacecorner    lindtobias\n"..
            "zaphod77    Arch Nemesis    dtet_enjoyer\n"..
            "woozy    Zircean    AgentBasey    Eden GT",
            300, other_text_y + 133, 400, "left"
        )
    elseif current_page == 2 then
        -- Main font
        drawBigText("Font", 0, 10, 250, "right")
        drawText("all used font are licensed under\nthe SIL Open Font License, v.1.1", 0, 40, 250, "right")
        drawText(
            "Iosevka - Renzhi Li (aka. Bellve Invis),\n(for game's UI interface and\n on-screen buttons' texture)\n\n"..
            "Exo 2 for Techmino - C₂₉H₂₅N₃O₅, (for game's UI)\n\n"..
            "Cascadia Code - Microsoft, used with Iosevka\nfor on-screen buttons' texture",
            300, 23, 340, "left"
        )
        -- VCTRL
        drawBigText("On-screen buttons\nimplementation", 0, 180, 250, "right")
        drawText("UI's buttons: using simple-button module\nwritten by SweetSea\n\nIn-game on-screen control module is based on MrZ's VCTRL module in his games\nTexture made by SweetSea", 300, 193, 340, "left")

        drawBigText("Special thanks", 25, 310, 460, "left")
        drawText(
"The Absolute Plus - theabsolute.plus      FYAD/Imp Zone Collective\
MrZ - https://github.com/MrZ626           C₂₉H₂₅N₃O₅ - https://github.com/C29H25N3O5\n\
                             AND ALL VERSION 1 PLAYERS                              "
        , 25, 350, 600, "left")
    end
end

---@param e SCENE_onInput
function AboutScene:onInputPress(e)
    local key = e.input or e.key
    if e.type == "touch" or e.type == "mouse" then
        BUTTON.press(buttonList, e.x, e.y, e.id)
    elseif key == "left"  or key == "up"   then changePage(-1)
    elseif key == "right" or key == "down" then changePage( 1)
    elseif e.input == "menu_back" or e.scancode == "escape" then
        SCENE = TitleScene()
    end
end
function AboutScene:onInputRelease(e)
    if e.type == "touch" or e.type == "mouse" then
        BUTTON.release(buttonList, e.x, e.y, e.id)
    end
end
function AboutScene:onInputMove(e)
    if e.type == "mouse" then
        BUTTON.checkHovering(buttonList, e.x, e.y)
    end
end

return AboutScene