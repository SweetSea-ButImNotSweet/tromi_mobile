local MusicToggleScene = SCENE:extend()
MusicToggleScene.title = "Play music during game:"

function MusicToggleScene:update()
    SETTINGS["music"] = not SETTINGS["music"]
    SCENE = TitleScene()
end

return MusicToggleScene