local LoadingScene = SCENE:extend()
local LoadingImageFile = love.graphics.newImage('res/loading.png')

function LoadingScene.update()
    if LOADED_BACKGROUND and LOADED_MUSIC then
        SCENE = SETTINGS.firstTime and InputConfigScene(true) or TitleScene()
        LILY.quit()
    end
end
function LoadingScene.render()
    love.graphics.draw(LoadingImageFile,0,0,0,0.5)
    drawText(
        ("Background: %s\nMusic & SFX: %s"):format(
            LOADED_BACKGROUND and "loaded" or "...",
            LOADED_MUSIC and "loaded" or "..."
        ),
        15, 400, 1e99, "left"
    )
end

return LoadingScene