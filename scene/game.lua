local GameScene = SCENE:extend()
GameScene.title = "Game"

local tas = false

local buttonList = {
    BUTTON.new{
        text = "Rotate Left\n   Pause/Frame Step",
        x = 67, y = 289, h = 40, w = 140,
        backgroundColor = {0, 0, 0, 0},
        borderColor = {0, 0, 0, 0},
        hoverColor = { 1, 1, 1, 0.2},
        textOrientation = "left",
        codeWhenPressed = function() SCENE:onInputPress{input = "rotate_left"} end
    },
    BUTTON.new{
        text = "Rotate Right\n   Unpause",
        x = 67, y = 323, h = 40, w = 140,
        backgroundColor = {0, 0, 0, 0},
        borderColor = {0, 0, 0, 0},
        hoverColor = { 1, 1, 1, 0.2},
        textOrientation = "left",
        codeWhenPressed = function() SCENE:onInputPress{input = "rotate_right"} end
    },
    BUTTON.new{
        text = "Left\n   Rewind 5 sec",
        x = 67, y = 357, h = 40, w = 140,
        backgroundColor = {0, 0, 0, 0},
        borderColor = {0, 0, 0, 0},
        hoverColor = { 1, 1, 1, 0.2},
        textOrientation = "left",
        codeWhenPressed = function() SCENE:onInputPress{input = "left"} end
    },
    BUTTON.new{
        text = "Right\n   FF 10 sec",
        x = 67, y = 391, h = 40, w = 140,
        backgroundColor = {0, 0, 0, 0},
        borderColor = {0, 0, 0, 0},
        hoverColor = { 1, 1, 1, 0.2},
        textOrientation = "left",
        codeWhenPressed = function() SCENE:onInputPress{input = "right"} end
    },
}
local menuKey   -- MENU key used to go main menu XD

function GameScene:new(player_name, replay_file, replay_grade)
    menuKey = BUTTON.new{
        text = CHAR.icon.menu.." MENU",
        x = 10, y = 10, w = 70, h = 30,
        codeWhenReleased = function()
            if self.game.input_playback or self.game.game_over or self.game.completed then
                SCENE = TitleScene()
            end
        end
    }
    VCTRL[9].show = false
    BUTTON.reset(buttonList)

    game_mode = require 'game.gamemode'
    if PENTO_MODE then
        ruleset = require 'game.rotation_pent'
    else
        ruleset = require 'game.rotation'
    end
    
    self.retry_mode = game_mode
    self.retry_ruleset = ruleset
    -- self.secret_inputs = inputs
    self.reset_stuff = {player_name, replay_file, replay_grade}
    self.game = game_mode(player_name, replay_file, replay_grade)
    self.ruleset = ruleset(self.game)
    self.grace_frames = 0
    self.normal_volume = love.audio.getVolume()
    self.game:initialize(self.ruleset)
    self.inputs = {
        left=false,
        right=false,
        up=false,
        down=false,
        rotate_left=false,
        rotate_left2=false,
        rotate_right=false,
        rotate_right2=false,
        hold=false,
    }
    self.paused = false
end

function GameScene:update(nosound, tas_update)
    local inputs = {}
    if tas then
        while self.game.are > 2 do
            self.game:update(inputs, self.ruleset)
        end
    end
    for input, value in pairs(self.inputs) do
        inputs[input] = value
    end
    if tas and tas_update then
        self.paused = false
        self.game:update(inputs, self.ruleset)
        self.paused = true
        return
    end
    if nosound then
        love.audio.setVolume(0)
    end
    if not nosound and self.grace_frames > 0 then
        self.grace_frames = self.grace_frames - 1
        if self.grace_frames == 1 then love.audio.setVolume(self.normal_volume) end
    end
    if not self.paused then
        self.game:update(inputs, self.ruleset)
    end
    -- if self.game.input_playback or self.game.game_over or self.game.game_completed then
    --     VCTRL[9].show = true; VCTRL[10].show = true
    -- end
end

function GameScene:render()
    self.game:draw(self.paused)
    if self.game.input_playback then
        BUTTON.draw(buttonList)
    else
        VCTRL.draw()
    end
    if self.game.input_playback or self.game.game_over or self.game.completed then
        menuKey:draw()
    end
end

function GameScene:onInputPress(e)
    if e.type == "mouse" or (e.type == "touch" and not VCTRL.press(e.x, e.y, e.id)) then
        if self.game.input_playback then BUTTON.press(buttonList, e.x, e.y, e.id) end
        menuKey:press(e.x, e.y, e.id)
    elseif (self.game.game_over or self.game.completed) and (e.input == "menu_decide" or e.input == "menu_back" or e.input == "rotate_right") and self.game.game_over_frames > 50 then
        SCENE = TitleScene()
    elseif tas and e.input == "menu_decide" then
        self:update(false, true)
    elseif self.game.input_playback and (e.input == "menu_back") then
        SCENE = TitleScene()
    elseif self.game.input_playback and e.input == "rotate_left" then
        self.paused = false
        self:update()
        self.paused = true
    elseif self.game.input_playback and e.input == "rotate_right" then
        self.paused = false
    elseif self.game.input_playback and not self.paused and e.input == 'left' then
        local target = self.game.frames - 300
        if target < 1 then target = 1 end
        self.game = game_mode(self.reset_stuff[1], self.reset_stuff[2], self.reset_stuff[3])
        self.ruleset = ruleset(self.game)
        self.game:initialize(self.ruleset)
        while self.game.frames < (target) do
            self:update(true)
        end
        self.grace_frames = 90
    elseif self.game.input_playback and not self.paused and e.input == 'right' then
        local target = self.game.frames + 600
        if target > #self.game.replay_inputs then target = #self.game.replay_inputs-10 end
        -- self.game = game_mode(self.reset_stuff[1], self.reset_stuff[2], self.reset_stuff[3])
        self.ruleset = ruleset(self.game)
        self.game:initialize(self.ruleset)
        while self.game.frames < (target) do
            self:update(true)
        end
        self.grace_frames = 90
    elseif e.input and string.sub(e.input, 1, 5) ~= "menu_" then
        self.inputs[e.input] = true
    end
end

function GameScene:onInputRelease(e)
    if e.type == "mouse" or (e.type == "touch" and not VCTRL.release(e.id)) then
        if self.game.input_playback then BUTTON.release(buttonList, e.x, e.y, e.id) end
        menuKey:release(e.x, e.y, e.id)
    elseif e.input and string.sub(e.input, 1, 5) ~= "menu_" then
        self.inputs[e.input] = false
    end
end

function GameScene:onInputMove(e)
    if e.type == "mouse" then BUTTON.checkHovering(buttonList, e.x, e.y) end
    menuKey._hovering = menuKey:isHovering(e.x, e.y)
end

return GameScene
