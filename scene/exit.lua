local ExitScene = SCENE:extend()
ExitScene.title = "Exit Game"

function ExitScene:new()
end

function ExitScene:update()
	love.event.quit()
end

return ExitScene

