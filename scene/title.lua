local TitleScene = SCENE:extend()

TitleScene.title = "Title"
TitleScene.restart_message = false

local main_menu_screens = {
    NameEntryScene,
    ReplaySelectScene,
    TrainingScene,
    InputConfigScene,
    FullscreenScene,
    MusicToggleScene,
    LinesToggleScene,
    ExitScene,
}

function TitleScene:new()
    if SOUNDS['bgm_firsthalf']:isPlaying() or SOUNDS['bgm_secondhalf']:isPlaying() or not SETTINGS["music"] then
        love.audio.stop()
    end
    self.main_menu_state = 1
    PENTO_MODE = false
    self.code = {0,0,0,0,0,0,0,0}
    VCTRL.toggle(false)
end

function TitleScene:update()
end

function TitleScene:drawCredits(top, left)
	love.graphics.setColor(0,0,0,0.7)
	love.graphics.rectangle("fill", top, left, 335, 345, 10, 10)
	drawText("Design & Programming  mycophobia ", 6+top, 6+left, 1000, "left")
	drawText("Programming           Cambridge contributors", 6+top, 21+left, 1000, "left")
	drawText("Music                 Jerry Martin", 6+top, 36+left, 1000, "left")
	drawText("                      Juraj Stanik", 6+top, 51+left, 1000, "left")
	drawText("RNG Consultant        colour_thief", 6+top, 66+left, 1000, "left")
	drawText("Mac Launcher          nightmareci", 6+top, 81+left, 1000, "left")
	drawText("People Who Tested     switchpalacecorner", 6+top, 106+left, 1000, "left")
	drawText("and/or Offered        esquatre", 6+top, 121+left, 1000, "left")
	drawText("Cool Suggestions      Kirby703", 6+top, 136+left, 1000, "left")
	drawText("                      netdoll", 6+top, 151+left, 1000, "left")
	drawText("                      lindtobias", 6+top, 166+left, 1000, "left")
	drawText("                      zaphod77", 6+top, 181+left, 1000, "left")
	drawText("                      Arch Nemesis", 6+top, 196+left, 1000, "left")
	drawText("                      dtet_enjoyer", 6+top, 211+left, 1000, "left")
	drawText("                      woozy", 6+top, 226+left, 1000, "left")
	drawText("                      AgentBasey", 6+top, 241+left, 1000, "left")
	drawText("                      Zircean", 6+top, 256+left, 1000, "left")
	drawText("                      Eden GT", 6+top, 271+left, 1000, "left")
	drawText("Special Thanks        theabsolute.plus", 6+top, 291+left, 1000, "left")
	drawText("                      FYAD/Imp Zone Collective", 6+top, 306+left, 1000, "left")
	drawText("                      All Version 1 Players", 6+top, 321+left, 1000, "left")
end

function TitleScene:render()
    MainBackground()
    love.graphics.setColor(0,0,0,0.7)
    love.graphics.rectangle("fill", 14, 174, 260, 210, 10, 10)
    love.graphics.setColor(0.4, 1, 1, 0.5)
    love.graphics.rectangle("fill", 20, 198 + 20 * self.main_menu_state, 240, 22)
    --
    drawBigText('Tromi', 30, 180, 120, "left")
    drawText('version 2', 110, 193, 120, "left")
    --
    for i, screen in pairs(main_menu_screens) do
        drawText(screen.title, 40, 200 + 20 * i, 1200, "left")
    end
    --
    drawText(SETTINGS["music"] and "On" or "Off", 230, 320, 1000)
    drawText(SETTINGS["lines"] and "On" or "Off", 230, 340, 1000)

	love.graphics.setColor(0,0,0,0.7)
	love.graphics.rectangle("fill", 14, 400, 605, 75, 10, 10)
    --
    drawText("mycophobia.org", 20, 405, 1000)
    drawText("Based on Cambridge - t-sp.in/cambridge", 20, 420, 1000)
    drawText("Music for Tromi by Jerry Martin, all rights reserved - jerrymartinmusic.com", 20, 435, 1000)
    drawText("Game backgrounds by Pixabay users Joe_hackney, yokim, Favorisxp, Any_Ann, VisualSkyFX ", 20, 450, 1000)
    self:drawCredits(300, 40)
    
    if table.concat(self.code, ',') == '1,1,1,1,-1,-1,-1,-1' then PENTO_MODE = true end
    if PENTO_MODE then
        drawBigText('PENT MODE', 30, 100, 120, "left")
    end
end

function TitleScene:changeOption(rel)
    local len = #main_menu_screens
    self.main_menu_state = (self.main_menu_state + len + rel - 1) % len + 1
end

function TitleScene:onInputPress(e)
    if e.type == "touch" then
        if      -- PENTO mode
            e.x >= 14 and 
            e.y >= 40 and
            e.x <= 274 and
            e.y <= 160
        then
            if e.x >= 137 then -- Right
                table.remove(self.code, 8)
                table.insert(self.code, 1, 1)
            else -- Left
                table.remove(self.code, 8)
                table.insert(self.code, 1, -1)
            end
        else    -- Select option from menu
            local selecting = math.floor((e.y - 198) / 20)
            
            if (e.x >= 20 and e.x <= 260) and
            (selecting > 0 and selecting <= #main_menu_screens)
            then
                if self.main_menu_state ~= selecting then
                    self.main_menu_state = selecting
                else
                    SCENE = main_menu_screens[selecting]()
                end
            end
        end
    else
        if e.input == "menu_decide" or e.input == "rotate_left" or e.scancode == "return" then
            SCENE = main_menu_screens[self.main_menu_state]()
        elseif e.input == "up" or e.scancode == "up" then
            self:changeOption(-1)
        elseif e.input == "down" or e.scancode == "down" then
            self:changeOption(1)
        elseif e.input == "left" or e.scancode == "left" then
            table.remove(self.code, 8)
            table.insert(self.code, 1, -1)
        elseif e.input == "right" or e.scancode == "right" then
            table.remove(self.code, 8)
            table.insert(self.code, 1, 1)
        end
    end
end

function TitleScene:onInputRelease(e)
end

return TitleScene
