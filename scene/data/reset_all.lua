local ResetAllScene = SCENE:extend()
ResetAllScene.title = "Erase all high scores"

local selection_title = {"No", "Yes"}
local selection_icon = {
    CHAR.icon.crossMark,
    CHAR.icon.checkMark,
}
local menu_tip = "ARE YOU SURE you want to RESET ALL?\n\nCAUTION: This will reset everything: accounts, replays, high scores and your settings!\n\nGame will restart after resetting."

local function recursivelyDelete(item)
    if love.filesystem.getInfo(item,"directory") then
        for _, child in ipairs(love.filesystem.getDirectoryItems(item)) do
            recursivelyDelete(item..'/'..child)
            love.filesystem.remove(item..'/'..child)
        end
    elseif love.filesystem.getInfo(item) then
        love.filesystem.remove(item)
    end
    love.filesystem.remove(item)
end

local settings_func = {
    function() return DataManagementScene() end,
    function()
        recursivelyDelete('')
        love.window.showMessageBox("Done", "All your data erased\nQuitting game... Game will restart now!")
        love.event.quit()
    end,
}

function ResetAllScene:new()
    self.settings_menu_state = 1
end

function ResetAllScene:changeOption(rel)
    local len = #selection_title
    self.settings_menu_state = (self.settings_menu_state + len + rel - 1) % len + 1
end

function ResetAllScene:render()
    MainBackground()

    love.graphics.setColor(0, 0, 0, 0.7)
    love.graphics.rectangle("fill", 30,  60, 580,  85, 10, 10)  -- Tromi
    love.graphics.rectangle("fill", 30, 165, 580, 225, 10, 10)  -- Menu
    drawBigText(CHAR.icon.erase.." RESET ALL?", 40, 85, 400, "left")

    -- Selecting
    love.graphics.setColor(0.4, 1, 1, 0.5)
    love.graphics.rectangle("fill", 330, 135 + 40 * self.settings_menu_state, 270, 40)
    -- Text
    for i = 1, #selection_title do
        drawText   (selection_title[i], 365, 145 + 40 * i, 230, "left")
        drawBigText(selection_icon [i], 335, 135 + 40 * i,  50, "left")
    end

    drawText(menu_tip, 45, 175, 610 - 15 - 330, "left")
end

---@param e SCENE_onInput
function ResetAllScene:onInputPress(e)
    if e.input == "menu_back" or e.scancode == "escape" then SCENE = TitleScene()
    elseif e.input == "menu_decide" or e.key == "return" then
        local s = settings_func[self.settings_menu_state]()
        if s then SCENE = s end
    elseif e.input == "down" or e.scancode == "down" then
        self:changeOption(1)
    elseif e.input == "up" or e.scancode == "up" then
        self:changeOption(-1)

    elseif e.type == "touch" or e.type == "mouse" then
        local x, y = e.x, e.y
        local testOption = function(sel)
            if sel ~= self.settings_menu_state then
                self.settings_menu_state = sel
            else
                self:onInputPress{input = "menu_decide"}
            end
        end
        if (
            x >= 330 and x <= 600 and
            y >= 175 and y <= 375
        ) then
            local sel = math.floor((y - 175) / 40) + 1
            if sel <= #settings_func then
                testOption(sel)
            end
        end
    end
end

return ResetAllScene
