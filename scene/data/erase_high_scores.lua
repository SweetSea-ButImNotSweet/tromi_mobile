local EraseHighScoresScene = SCENE:extend()
EraseHighScoresScene.title = "Erase all high scores"

local selection_title = {"No", "Yes"}
local selection_icon = {
    CHAR.icon.crossMark,
    CHAR.icon.checkMark,
}
local menu_tip = "Are you sure you want to erase all your high scores?\n\nCAUTION!\nThis action CANNOT BE UNDONE!"

local settings_func = {
    function() return DataManagementScene() end,
    function()
        love.filesystem.remove(HIscoreFILE)
        love.window.showMessageBox("Done", "All high scores are erased!")
        return DataManagementScene()
    end,
}

function EraseHighScoresScene:new()
    self.settings_menu_state = 1
end

function EraseHighScoresScene:changeOption(rel)
    local len = #selection_title
    self.settings_menu_state = (self.settings_menu_state + len + rel - 1) % len + 1
end

function EraseHighScoresScene:render()
    MainBackground()

    love.graphics.setColor(0, 0, 0, 0.7)
    love.graphics.rectangle("fill", 30,  60, 580,  85, 10, 10)  -- Tromi
    love.graphics.rectangle("fill", 30, 165, 580, 225, 10, 10)  -- Menu
    drawBigText(CHAR.icon.erase.." ERASE ALL HIGH SCORES?", 40, 85, 400, "left")

    -- Selecting
    love.graphics.setColor(0.4, 1, 1, 0.5)
    love.graphics.rectangle("fill", 330, 135 + 40 * self.settings_menu_state, 270, 40)
    -- Text
    for i = 1, #selection_title do
        drawText   (selection_title[i], 365, 145 + 40 * i, 230, "left")
        drawBigText(selection_icon [i], 335, 135 + 40 * i,  50, "left")
    end

    drawText(menu_tip, 45, 175, 610 - 15 - 330, "left")
end

---@param e SCENE_onInput
function EraseHighScoresScene:onInputPress(e)
    if e.input == "menu_back" or e.scancode == "escape" then SCENE = TitleScene()
    elseif e.input == "menu_decide" or e.key == "return" then
        local s = settings_func[self.settings_menu_state]()
        if s then SCENE = s end
    elseif e.input == "down" or e.scancode == "down" then
        self:changeOption(1)
    elseif e.input == "up" or e.scancode == "up" then
        self:changeOption(-1)

    elseif e.type == "touch" or e.type == "mouse" then
        local x, y = e.x, e.y
        local testOption = function(sel)
            if sel ~= self.settings_menu_state then
                self.settings_menu_state = sel
            else
                self:onInputPress{input = "menu_decide"}
            end
        end
        if (
            x >= 330 and x <= 600 and
            y >= 175 and y <= 375
        ) then
            local sel = math.floor((y - 175) / 40) + 1
            if sel <= #settings_func then
                testOption(sel)
            end
        end
    end
end

return EraseHighScoresScene
