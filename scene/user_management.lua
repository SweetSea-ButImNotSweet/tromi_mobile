local UserManagementScene = SCENE:extend()

local max_items_per_page = 10
local gradeNames = {
    "19k", "18k", "17k", "16k", "15k", "14k", "13k", "12k", "11k",
    "10k", " 9k", " 8k", " 7k", " 6k", " 5k", " 4k", " 3k", " 2k", " 1k",
    " 1D", " 2D", " 3D", " 4D", " 5D", " 6D", " 7D", " 8D", " 9D"
}

local user_list
local buttonList = {
    BUTTON.new{
        text = CHAR.icon.home.." Home\nEsc/Go back",
        x = 510, y = 45, w = 120, h = 55,
        codeWhenPressed = function() SCENE = TitleScene() end
    },
    BUTTON.new{
        text = CHAR.icon.trash.." Delete\nDel/Rotate right",
        x = 510, y = 105, w = 120, h = 55,
        codeWhenPressed = function() SCENE:onInputPress{key = "delete"} end
    },
    BUTTON.new{
        text = CHAR.key.up.." Page up\nLeft",
        x = 510, y = 165, w = 120, h = 55,
    },
    BUTTON.new{
        text = CHAR.key.down.." Page down\nRight",
        x = 510, y = 225, w = 120, h = 55,
    }
}

function UserManagementScene:new()
    user_list = {}
    self.current_page = 1
    self.direction = nil
    self.selecting = 1
    self.repeat_counter = max_items_per_page - 1

    for _, f in pairs(love.filesystem.getDirectoryItems(SAVE_DIR)) do
        local file = SAVE_DIR .. f
        local info = love.filesystem.getInfo(file)

        if info and info.type == "file" and string.sub(f, 4, -1) == "_grade_history.sav" then
            local name = string.sub(f, 1, 3)
            table.insert(user_list, {
                name  = name,
                valid = false, -- Data is not broken?
                grade = 1,     -- Player's grade
                promo = 2,     -- Promotion Meter
                games = 0,     -- Games played
            })
            local okay, _ = pcall(function()
                local data                  = FILE.read(file)
                if not data then error() end
                user_list[#user_list].grade = data[1]
                user_list[#user_list].promo = data[2]
                user_list[#user_list].games = data[4]
            end)

            if okay then
                user_list[#user_list].valid = true
            end
        end
    end
end

local function getPromotionText(grade, promotion)
    local points_text
    if grade == 1 then
        points_text = ({
            [2] = "  |..",
            [3] = "  .|.",
            [4] = "  ..|",
        })[promotion]
    else
        points_text = ({
            [0] = "|....",
            [1] = ".|...",
            [2] = "..|..",
            [3] = "...|.",
            [4] = "....|",
        })[promotion]
    end
    return (grade > 1 and "-" or " ")..points_text..(grade < 29 and "+" or "")
end

function UserManagementScene:changeOption(rel)
    local len = #user_list
    self.selecting = self.selecting + rel
    if self.selecting + ((self.current_page-1) * max_items_per_page) > len then
        self.current_page = 1
        self.selecting = 1
    elseif self.selecting < 1 and self.current_page == 1 then
        self.current_page = 1 + (math.floor(len / max_items_per_page))
        self.selecting = len - ((self.current_page - 1) * max_items_per_page)
    end
    if self.selecting > max_items_per_page then
        self.current_page = self.current_page + 1
        self.selecting = 1
    elseif self.selecting < 1 then
        self.current_page = self.current_page - 1
        self.selecting = max_items_per_page
    end
end

function UserManagementScene:deleteCurrentReplay()
    local name = user_list[max_items_per_page * (self.current_page - 1) + self.selecting].name
    if love.window.showMessageBox(
        "Delete the selected profile?",
        "Are you sure you want to delete " .. string.upper(name) .. "'s profile?",
        {
            "Delete", ">>>CANCEL<<<",
            escapebutton = 2
        },
        "warning"
    ) == 1 then
        if love.window.showMessageBox(
            "Are you really sure???",
            "You really want to erase " .. string.upper(name) .. "'s profile?????\nYou may REGRET your action. This is NOT INVERSIBLE!",
            {
                ">>>CANCEL<<<", "Delete",
                escapebutton = 1
            },
            "warning"
        ) == 2 then
            love.filesystem.remove(SAVE_DIR .. name .. "_grade_history.sav")
            self:new()
        end
    end
end

function UserManagementScene:render()
    MainBackground()

    love.graphics.setColor(0, 0, 0, 0.7)
    love.graphics.rectangle("fill", 0, 0, 640, 480)
    love.graphics.setColor(0.4, 1, 1, 0.5)
    love.graphics.rectangle("fill", 0, 15 + 30 * self.selecting, 500, 27)

    BUTTON.draw(buttonList)

    drawText('Name - Grade - Pro.Meter - Games', 40, 20, 1000, "left")
    --        SEA      19k    -..|..+    12345
    for i = (self.current_page - 1) * max_items_per_page + 1, math.min((self.current_page) * max_items_per_page, #user_list) do
        if user_list[i].valid then
            local name  = user_list[i].name
            local games = string.format("%5d", user_list[i].games)
            local grade = gradeNames[user_list[i].grade]
            local promo = getPromotionText(user_list[i].grade, user_list[i].promo)
            drawText(
                " "   ..
                name  .. "    "  ..
                grade .. "     " ..
                promo .. "    "  ..
                games,
                40, 20 + 30 * (i - (self.current_page - 1) * max_items_per_page), 1000, "left"
            )
        else
            drawText(
                " " .. user_list[i].name.."    ???     -?????+    ?????",
                40, 20 + 30 * (i - (self.current_page - 1) * max_items_per_page), 1000, "left"
            )
        end
    end


    drawText(string.format('Page %s/%s', self.current_page, math.floor(#user_list / max_items_per_page) + 1), 15, 440, 100,
        "center")
end

function UserManagementScene:update()
    if self.direction == "up" then
        if self.repeat_counter >= max_items_per_page then
            self:changeOption(-1)
            self.repeat_counter = 0
        end
        self.repeat_counter = self.repeat_counter + 1
    elseif self.direction == "down" then
        if self.repeat_counter >= max_items_per_page then
            self:changeOption(1)
            self.repeat_counter = 0
        end
        self.repeat_counter = self.repeat_counter + 1
    end
end

---@param e SCENE_onInput
function UserManagementScene:onInputPress(e)
    if     e.type  == "mouse"        or e.type == "touch"  then
        if not BUTTON.press(buttonList, e.x, e.y, e.id) then
            local selection = math.floor((e.y - 45) / 30) + 1
            love.window.showMessageBox("", selection)
            if (
                selection >= 1 and
                selection <= self.repeat_counter and
                selection + ((self.current_page-1) * max_items_per_page) <= #user_list
            ) then
                self.selecting = selection
            end
        end
    elseif e.input == "menu_back"                          then SCENE = TitleScene()
    elseif e.input == "up"           or e.key  == "up"     then self.direction = "up"
    elseif e.input == "down"         or e.key  == "down"   then self.direction = "down"
    elseif e.input == "left"         or e.key  == "left"   then
        if self.current_page == 1 then
            self.current_page = 1 + math.floor(#user_list / max_items_per_page)
        else
            self.current_page = self.current_page - 1
        end
        self.selecting = 1
    elseif e.input == "right"        or e.key  == "right"  then
        if self.current_page < 1 + math.floor(#user_list / max_items_per_page) then
            self.current_page = self.current_page + 1
        else
            self.current_page = 1
        end
        self.selecting = 1
    elseif e.input == "rotate_right" or e.key  == "delete" then self:deleteCurrentReplay()
    end
end

function UserManagementScene:onInputRelease(e)
    if e.type == "mouse" or e.type == "touch" then
        BUTTON.release(buttonList, e.x, e.y, e.id)
    elseif e.input == "up" or e.scancode == "up" or e.input == "down" or e.scancode == "down" then
        self.direction = nil
        self.repeat_counter = max_items_per_page - 1
    end
end

function UserManagementScene:onInputMove(e)
    if e.type == "mouse" then
        BUTTON.checkHovering(buttonList, e.x, e.y)
    end
end

return UserManagementScene
