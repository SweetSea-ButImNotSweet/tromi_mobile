local ReplaySelectScene = SCENE:extend()
ReplaySelectScene.title = "Replay"

local replay_list
local buttonList = {
    BUTTON.new{
        x = 325, y = 375, w = 100, h = 30,
        text = CHAR.key.up.." Page up",
        codeWhenReleased = function() SCENE:onInputPress{input = "left"} end
    },
    BUTTON.new{
        x = 435, y = 375, w = 100, h = 30,
        text = CHAR.key.down.." Page down",
        codeWhenReleased = function() SCENE:onInputPress{input = "right"} end
    },
    BUTTON.new{
        x = 105, y = 375, w = 100, h = 30,
        text = CHAR.icon.play.." Play",
        codeWhenPressed  = function() SCENE:onInputPress  {input = "menu_decide"} end,
        codeWhenReleased = function() SCENE:onInputRelease{input = "menu_decide"} end
    },
    BUTTON.new{
        x = 105, y = 415, w = 100, h = 45,
        text = CHAR.icon.home.." Home",
        codeWhenPressed  = function() SCENE:onInputPress  {input = "menu_back"} end,
        codeWhenReleased = function() SCENE:onInputRelease{input = "menu_back"} end
    },
    BUTTON.new{
        x = 215, y = 415, w = 100, h = 45,
        text = CHAR.icon.save.." Converter",
        codeWhenReleased = function() love.system.openURL("https://sweetsea-butimnotsweet.github.io/tromi_replay_converter/") end
    },
    BUTTON.new{
        x = 325, y = 415, w = 100, h = 45,
        text = CHAR.icon.export.." Export\n(R.Left)",
        codeWhenPressed  = function() SCENE:onInputPress  {input = "rotate_left"} end,
        codeWhenReleased = function() SCENE:onInputRelease{input = "rotate_left"} end
    },
    BUTTON.new{
        x = 435, y = 415, w = 100, h = 45,
        text = CHAR.icon.import.." Import\n(R.Right)",
        codeWhenPressed  = function() SCENE:onInputPress  {input = "rotate_right"} end,
        codeWhenReleased = function() SCENE:onInputRelease{input = "rotate_right"} end
    },
}

function ReplaySelectScene:new()
    self:initList()
    SCENE = self
    BUTTON.reset(buttonList)
    PENTO_MODE = false
end

function ReplaySelectScene:initList()
    self.replays = {}
    replay_list = love.filesystem.getDirectoryItems('saves/replays/')
    table.sort(replay_list, function(a,b) return a > b end)
    self.replay_text = {}
    self.page_flip = 10
    self.page = 1
    local gradeNames = {
        "19k", "18k", "17k", "16k", "15k", "14k", "13k", "12k", "11k",
        "10k", "9k", "8k", "7k", "6k", "5k", "4k", "3k", "2k", "1k",
        "1D", "2D", "3D", "4D", "5D", "6D", "7D", "8D", "9D"
        }
    for i=1, #replay_list do
        if string.find(replay_list[i], "_replay.sav") ~= nil then
            table.insert(self.replays, replay_list[i])
            local line_components = {}
            for str in string.gmatch(replay_list[i], "([^".."_".."]+)") do
                table.insert(line_components, str)
            end
            local player_name = line_components[2]
            local player_grade = gradeNames[tonumber(line_components[3])]
            local player_score = line_components[4]
            if #player_grade == 2 then player_grade = ' '..player_grade end
            table.insert(self.replay_text, player_name..'  -   '..player_grade..' - '..string.format('%6d',player_score))
        end
    end
    self.replay_select = 1
    self.direction = nil
    self.repeat_limit = 10
    self.repeat_counter = self.repeat_limit-1
end

function ReplaySelectScene:render()
    MainBackground()

    love.graphics.setColor(0, 0, 0, 0.7)
    love.graphics.rectangle("fill", 0, 0, 640, 480)

    if #self.replays > 0 then
        love.graphics.setColor(0.4, 1, 1, 0.5)
        love.graphics.rectangle("fill", 0, 15 + 30 * self.replay_select, 640, 27)

        drawText('Name - Grade - Score', 40, 20, 1000, "left")
        drawText(string.format('Page %s/%s', self.page, 1 + math.floor((#self.replays - 1) / self.page_flip)), 215, 380, 100, "center")

        local i, j = 1, 1
        while i <= #self.replay_text do
            if j > self.page_flip then j = 1
            elseif j < 1 then j = self.page_flip
            end
            if i > (self.page-1) * self.page_flip and i <= self.page * self.page_flip then
                drawText(self.replay_text[i], 40, 20 + 30 * j, 1000, "left")
            end
            j = j + 1
            i = i + 1
        end
    else
        drawText('No replays yet!', 40, 40, 1000, "left")
    end

    BUTTON.draw(buttonList)
end

function ReplaySelectScene:update()
    if self.direction == "up" then
        if self.repeat_counter >= self.repeat_limit then
            self:changeOption(-1)
            self.repeat_counter = 0
        end
        self.repeat_counter = self.repeat_counter + 1
    elseif self.direction == "down" then
        if self.repeat_counter >= self.repeat_limit then
            self:changeOption(1)
            self.repeat_counter = 0
        end
        self.repeat_counter = self.repeat_counter + 1
    end
end

function ReplaySelectScene:changeOption(rel)
    local len = #self.replays
    self.replay_select = self.replay_select + rel
    if self.replay_select + ((self.page-1) * self.page_flip) > len then
        self.page = 1
        self.replay_select = 1
    elseif self.replay_select < 1 and self.page == 1 then
        self.page = 1+(math.floor(len / self.page_flip))
        self.replay_select = len - ((self.page-1) * self.page_flip)
    end
    if self.replay_select > self.page_flip then
        self.page = self.page + 1
        self.replay_select = 1
    elseif self.replay_select < 1 then
        self.page = self.page - 1
        self.replay_select = self.page_flip
    end
end

function ReplaySelectScene:onInputPress(e)
    local selected_replay = self.replays[self.replay_select + ((self.page-1) * self.page_flip)]
    local selected_replay_text = self.replay_text[self.replay_select + ((self.page-1) * self.page_flip)]

    if (
        (e.type == "touch" or e.type == "mouse") and
        not BUTTON.press(buttonList, e.x, e.y, e.id) and
        #self.replays > 0
    ) then
        local selection = math.floor((e.y - 15) / 30)
        if (
            selection >= 1 and
            selection <= self.repeat_limit and
            selection + ((self.page-1) * self.page_flip) <= #replay_list
        ) then
            self.replay_select = selection
        end
    elseif e.input == "menu_decide" or e.scancode == "return" then
        if self.replays[1] == nil then SCENE = TitleScene(); return
        else
            local line_components = {}
            for str in string.gmatch(selected_replay_text, "([^".."-".."]+)") do
                table.insert(line_components, str)
            end
            local player_name = line_components[1]
            local player_grade = string.gsub(line_components[2], " ", "")
            SCENE = GameScene(player_name, selected_replay, player_grade)
        end
    elseif e.input == "rotate_left" then    -- Export
        local plain_replay_data = love.data.encode("string", "base64", love.filesystem.read(REPLAY_DIR..selected_replay))
        love.system.setClipboardText(("BEGIN_OF_TROMI_REPLAY|%s|%s|END_OF_TROMI_REPLAY"):format(selected_replay, plain_replay_data))
    elseif e.input == "rotate_right" then   -- Import
        local input_data = love.system.getClipboardText()
        if input_data:find("BEGIN_OF_TROMI_REPLAY|", 1, true) == 1 and input_data:find("|END_OF_TROMI_REPLAY", 1, true) == #input_data - 19 then
            local data_part = input_data:sub(27, #input_data - 20)
            local separator_pos = data_part:find("|", 1, true)
            local replay_name = data_part:sub(1, separator_pos - 1)
            local replay_path = REPLAY_DIR..replay_name
            local replay_data = love.data.decode("string", "base64", data_part:sub(separator_pos + 1))
            love.filesystem.write(replay_path, replay_data)

            BUTTON.reset(buttonList)
            SCENE = ReplayTestScene(replay_name)
        else
            BUTTON.reset(buttonList)
            SCENE = ReplayTestScene()
        end
    elseif e.input == "up" or e.scancode == "up" then
        if self.replays[1] == nil then SCENE = TitleScene(); return end
        self.direction = 'up'
    elseif e.input == "down" or e.scancode == "down" then
        if self.replays[1] == nil then SCENE = TitleScene(); return end
        self.direction = 'down'
    elseif e.input == "left" or e.scancode == "left" then
        if self.replays[1] == nil then SCENE = TitleScene(); return end
        if self.page == 1 then
            self.page = math.floor((#self.replays - 1) / self.page_flip)
        else
            self.page = self.page - 1
        end
        self.replay_select = 1;
    elseif e.input == "right" or e.scancode == "right" then
        if self.replays[1] == nil then SCENE = TitleScene(); return end
        if self.page < 1 + math.floor((#self.replays - 1) / self.page_flip) then
            self.page = self.page + 1
        else
            self.page = 1
        end
        self.replay_select = 1;
    elseif e.input == "menu_back" or e.scancode == "backspace" or e.scancode == "delete" then
        SCENE = TitleScene()
    end
end

function ReplaySelectScene:onInputRelease(e)
    if e.type == "touch" or e.type == "mouse" then
        BUTTON.release(buttonList, e.x, e.y, e.id)
    elseif e.input == "up" or e.scancode == "up" or e.input == "down" or e.scancode == "down" then
        self.direction = nil
        self.repeat_counter = self.repeat_limit-1
    end
end

function ReplaySelectScene:onInputMove(e)
    if e.type == "mouse" then
        BUTTON.checkHovering(buttonList, e.x, e.y)
    end
end

return ReplaySelectScene
