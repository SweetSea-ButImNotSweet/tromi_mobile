local KeyConfigScene = SCENE:extend()
KeyConfigScene.title = "Key Config"
local buttonList = {}

local configurable_inputs = {
    "menu_decide",
    "menu_back",
    "left",
    "right",
    "up",
    "down",
    "rotate_left",
    "rotate_left2",
    "rotate_right",
    "rotate_right2",
}
local input_names = {
    menu_decide='Confirm Selection',
    menu_back = 'Go Back',
    left='Left',
    right='Right',
    up='Up',
    down='Down',
    rotate_left='Rotate Counter-clockwise',
    rotate_left2='Rotate Counter-clockwise (2)',
    rotate_right='Rotate Clockwise',
    rotate_right2='Rotate Clockwise (2)'
}

local function newSetInputs()
    local set_inputs = {}
    for i, input in ipairs(configurable_inputs) do
        set_inputs[input] = false
    end
    return set_inputs
end

function KeyConfigScene:new()
    self.input_state = 1
    self.set_inputs = newSetInputs()
    self.new_input = {}
    self.axis_timer = 0

    BUTTON.reset(buttonList)
    buttonList = { -- Configuring
        BUTTON.new{
            text = CHAR.key.tab.."\nTab",
            x = 40, y = 300, w = 100, h = 50,
            codeWhenReleased = function() self:onInputPress{type = "key", scancode = "tab"} end
        },
        BUTTON.new{
            text = CHAR.key.enter_or_return.."\nEnter/Return",
            x = 150, y = 300, w = 100, h = 50,
            codeWhenReleased = function() self:onInputPress{type = "key", scancode = "return"} end
        },
        BUTTON.new{
            text = CHAR.key.del.."\nDelete",
            x = 260, y = 300, w = 100, h = 50,
            codeWhenReleased = function() self:onInputPress{type = "key", scancode = "delete"} end
        },
        BUTTON.new{
            text = CHAR.key.esc.."\nEscape",
            x = 370, y = 300, w = 100, h = 50,
            codeWhenReleased = function() self:onInputPress{type = "key", scancode = "escape"} end
        },
    }
end

function KeyConfigScene:update()
end

function KeyConfigScene:render()
    MainBackground()
    love.graphics.setColor(0, 0, 0, 0.7)
    love.graphics.rectangle("fill", 0, 0, 640, 480)
    BUTTON.draw(buttonList)

    for i, input in ipairs(configurable_inputs) do
        drawText(input_names[input], 40, 60 + i * 20, 200, "left")
        if self.set_inputs[input] then
            drawText(self.set_inputs[input], 240, 60 + i * 20, 300, "left")
        end
    end
    if self.input_state > #configurable_inputs then
        drawText("Press Enter/Confirm Selection to confirm, Delete/Backspace to retry" .. (SETTINGS.input.keys and ", Esc/Go Back to cancel" or ""), 0, 0, 1000)
    else
        drawText("Press key input for " .. input_names[configurable_inputs[self.input_state]] .. ", escape to cancel\nPress tab on keyboard, or any key from other inputs, to skip",0,0,1000)
        drawText("Function keys (F1, F2, etc.), Escape, and Tab can't be changed", 0, 35,1000)
    end
end

function KeyConfigScene:onInputPress(e)
    if e.type == "mouse" or e.type == "touch" then
        BUTTON.press(buttonList, e.x, e.y, e.id)
    elseif e.type == "key" then
        -- function keys, escape, and tab are reserved and can't be remapped
        if e.scancode == "escape" or (self.input_state > #configurable_inputs and e.input == "menu_back") then
            SCENE = InputConfigScene(SETTINGS.firstTime)
        elseif self.input_state > #configurable_inputs then
            if e.scancode == "return" or e.input == "menu_decide" then
                SETTINGS.input.keys = self.new_input
                SCENE = SETTINGS.firstTime and TitleScene() or InputConfigScene()
                SETTINGS.firstTime = false
            elseif e.scancode == "delete" or e.scancode == "backspace" then
                self:new() -- retry
            end
        elseif e.scancode == "tab" then
            self.set_inputs[configurable_inputs[self.input_state]] = "skipped"
            self.input_state = self.input_state + 1
        elseif not self.new_input[e.scancode] then
            -- all other keys can be configured
            self.set_inputs[configurable_inputs[self.input_state]] = "key " .. love.keyboard.getKeyFromScancode(e.scancode) .. " (" .. e.scancode .. ")"
            self.new_input[e.scancode] = configurable_inputs[self.input_state]
            self.input_state = self.input_state + 1
        end
    elseif self.input_state <= #configurable_inputs then
        self.set_inputs[configurable_inputs[self.input_state]] = "skipped"
        self.input_state = self.input_state + 1
    end
end

function KeyConfigScene:onInputRelease(e)
    if e.type == "mouse" or e.type == "touch" then
        BUTTON.release(buttonList, e.x, e.y, e.id)
    end
end

function KeyConfigScene:onInputMove(e)
    if e.type == "mouse" then
        BUTTON.checkHovering(buttonList, e.x, e.y)
    end
end

return KeyConfigScene
