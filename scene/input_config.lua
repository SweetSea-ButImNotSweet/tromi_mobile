local ConfigScene = SCENE:extend()
ConfigScene.title = "Input Config"

local menu_screens = {
    KeyConfigScene,
    StickConfigScene,
    TouchConfigScene,
}

local buttonList
local function updateButtonList(self)
    buttonList = {
        BUTTON.new{
            text = "1", font = FONT_big,
            x = 75, y = 160, w = 40, h = 40,
            codeWhenReleased = function()
                if self.menu_state ~= 1 then
                    self.menu_state = 1
                else
                    SCENE = KeyConfigScene()
                end
            end
        },
        BUTTON.new{
            text = "2", font = FONT_big,
            x = 75, y = 200, w = 40, h = 40,
            codeWhenReleased = function()
                if self.menu_state ~= 2 then
                    self.menu_state = 2
                else
                    SCENE = StickConfigScene()
                end
            end
        },
        BUTTON.new{
            text = "3", font = FONT_big,
            x = 75, y = 240, w = 40, h = 40,
            codeWhenReleased = function()
                if self.menu_state ~= 3 then
                    self.menu_state = 3
                else
                    SCENE = TouchConfigScene()
                end
            end
        }
    }
    if not SETTINGS.firstTime then
        menu_screens[4] = TitleScene
        buttonList[4] = BUTTON.new{
            text = CHAR.icon.home, font = FONT_big,
            x = 75, y = 280, w = 40, h = 40,
            codeWhenReleased = function()
                if self.menu_state ~= 4 then
                    self.menu_state = 4
                else
                    SCENE = TitleScene()
                end
            end
        }
    else
        menu_screens[4] = nil
        buttonList[4] = nil
    end
end

local secret_code_input = {}
local secret_code_used = false

function ConfigScene:new(first_time)
    updateButtonList(self)

    secret_code_used = false
    secret_code_input = {}     -- When it matches 88663366 then we will automatically set the special keybind

    self.menu_state = 1
    if first_time then
        self.first_time = true
    else
        self.first_time = false
    end
end

function ConfigScene:update() end

function ConfigScene:render()
    MainBackground()
    love.graphics.setColor(0, 0, 0, 0.7)
    love.graphics.rectangle("fill", 0, 0, 640, 480)

    if secret_code_used then
        if SETTINGS.tvMode then
            drawText("TV mode is ON now! Check keybind below", 80, 40, 1000)
            drawText("Which controls do you want to configure?", 80, 70, 1000)
            drawText(
                "2 - Up          1 - Rotate left         5 - Confirm selection\n"..
                "8 - Down        3 - Rotate right        0 - Back\n"..
                "4 - Left        7 - Rotate left  2\n"..
                "6 - Right       9 - Rotate right 2", 80, 350, 1000
            )
        else
            drawText("TV mode is OFF now!", 80, 40, 1000)
            drawText("Which controls do you want to configure?", 80, 70, 1000)
            end
        elseif self.first_time then
            drawText("Thanks for playing Tromi!", 80, 40, 1000)
            drawText("Please begin by configuring your controls:", 80, 70, 1000)
        else
            drawText("Which controls do you want to configure?", 80, 70, 1000)
        end
    drawBigText(table.concat(secret_code_input, " "), 80, 100, 1000)

    love.graphics.setColor(1, 1, 1, 0.5)
    love.graphics.rectangle("fill", 75, 120 + 40 * self.menu_state, 300, 40)

    love.graphics.setColor(1, 1, 1, 1)
    for i, screen in pairs(menu_screens) do
        drawText(screen.title, 130, 130 + 40 * i, 300, "left")
    end
    BUTTON.draw(buttonList)
end

function ConfigScene:changeOption(rel)
    local len = #menu_screens
    self.menu_state = (self.menu_state + len + rel - 1) % len + 1
end

function ConfigScene:onInputMove(e)
    if e.type == "mouse" then
        BUTTON.checkHovering(buttonList, e.x, e.y)
    end
end

---@param key string
local function checkSecretCodeInput(key)
    if secret_code_used then return end
    if key:sub(1, 2) == "kp" then
        table.insert(secret_code_input, key:sub(3,3))
    elseif key:find("[0-9]") == 1 then
        table.insert(secret_code_input, key)
    else
        secret_code_input = {}      -- Reset
    end

    if #secret_code_input > 8 then
        table.remove(secret_code_input, 1)
    end

    local current_code = table.concat(secret_code_input, "")
    if current_code == "88663366" then      --TVMODEON
        -- Set keybind
        SETTINGS.input.keys = {
            ["2"] = "up",               ["kp8"] = "up",
            ["8"] = "down",             ["kp2"] = "down",
            ["4"] = "left",             ["kp4"] = "left",
            ["6"] = "right",            ["kp6"] = "right",
            ["1"] = "rotate_left",      ["kp7"] = "rotate_left",
            ["3"] = "rotate_right",     ["kp9"] = "rotate_right",
            ["7"] = "rotate_left2",     ["kp1"] = "rotate_left2",
            ["9"] = "rotate_right2",    ["kp3"] = "rotate_right2",
            ["5"] = "menu_decide",      ["kp5"] = "menu_decide",
            ["0"] = "menu_back",        ["kp0"] = "menu_back",
        }
        SETTINGS.firstTime = false
        SETTINGS.tvMode = true
        secret_code_used = true
        updateButtonList(SCENE)
    elseif current_code == "11111111" then
        SETTINGS.input.keys = {}
        SETTINGS.tvMode = false
        secret_code_used = true
    end
end

---@param e SCENE_onInput
function ConfigScene:onInputPress(e)
    if e.type == "touch" or e.type == "mouse" then
        BUTTON.press(buttonList, e.x, e.y, e.id)
    elseif e.input == "menu_decide" or e.input == "rotate_left" or e.scancode == "return" then
        SCENE = menu_screens[self.menu_state]()
    elseif e.input == "up" or e.scancode == "up" then
        self:changeOption(-1)
    elseif e.input == "down" or e.scancode == "down" then
        self:changeOption(1)
    elseif not SETTINGS.firstTime and (
        e.input == "menu_back" or e.input == "rotate_right" or e.scancode == "backspace" or e.scancode == "delete"
    ) then
        SCENE = TitleScene()
    end
    checkSecretCodeInput(e.key or "")
end
function ConfigScene:onInputRelease(e)
    if e.type == "touch" or e.type == "mouse" then
        BUTTON.release(buttonList, e.x, e.y, e.id)
    end
end

return ConfigScene
