local Object = require "libs.classic"

---@class SCENE
---@field title string
---@field extend function # Get a empty SCENE table
---
---@field new function
---@field update function
---@field render function
---@field onInputMove function
---@field onInputPress function
---@field onInputRelease function

SCENE = Object:extend()

function SCENE:new() end
function SCENE:update() end
function SCENE:render() end

-- You can use the class SCENE_onInput to show suggestions for `e` table

---@class SCENE_onInput
---@field type? "key"|"joystick"|"virtual"|"touch"|"mouse"|"wheel"
---
---@field input? # Action triggered<br>Only visible via keyboard and gamepad
    ---| "menu_decide"
    ---| "menu_back"
    ---| "left"
    ---| "right"
    ---| "up"
    ---| "down"
    ---| "rotate_left"
    ---| "rotate_right"
    ---| "rotate_left2"
    ---| "rotate_right2"
---
---@field key? love.KeyConstant Key pressed? Only visible via keyboard and gamepad
---@field scancode? love.Scancode Key pressed but on the US layout? Only visible via keyboard and gamepad
---
---@field x? number Only visible via touch and mouse
---@field y? number Only visible via touch and mouse
---@field dx? number # Delta X<br> Only visible via touch, mouse and wheel
---@field dy? number # Delta Y<br> Only visible via touch, mouse and wheel
---@field id? lightuserdata # Only visible via touch
---@field presses? number # Only visible via mouse

-- e in 4 below functions will contain different things based on it's type:
--  key      - input, key, scancode
--  joystick - input, button, name
--  virtual  - input
--  touch    - x, y, dx, dy, id
--  mouse    - x, y, dx, dy, presses
--  wheel    -       dx, dy

function SCENE:onInputMove(e) end
function SCENE:onInputPress(e) end
function SCENE:onInputRelease(e) end

LoadingScene = require "scene.loading"

GameScene = require "scene.game"
TrainingScene = require "scene.training"
NameEntryScene = require "scene.name_entry"

FullscreenScene = require "scene.fullscreen"
KeyConfigScene = require "scene.key_config"
StickConfigScene = require "scene.stick_config"
-- MusicToggleScene = require "scene.music_toggle"
-- LinesToggleScene = require "scene.lines_toggle"

TouchConfigScene = require "scene.touch_config"
TouchConfigPreviewScene = require "scene.touch_config_preview"
InputConfigScene = require "scene.input_config"
SettingsScene = require "scene.settings"

EraseHighScoresScene = require "scene.data.erase_high_scores"
ResetAllScene = require "scene.data.reset_all"
DataManagementScene = require "scene.data_management"
UserManagementScene = require "scene.user_management"

ReplaySelectScene = require "scene.replay"
ReplayTestScene = require"scene.replay_test"

AboutScene = require "scene.about"
ExitScene = require "scene.exit"
TitleScene = require "scene.title2"