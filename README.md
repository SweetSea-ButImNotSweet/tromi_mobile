# Tromi (ANDROID port)
A old-fashioned but modern block-stacking game. Made with :heart: by [mycophobia](https://mycophobia.org/tromi).<br>
Ported to Android (mobile and TV) by SweetSea with on-screen control (with some necessary changes)

# How to play
> :warning: There is no Android build (aka APK file) right now. If you see any pre-built APK files on the internet. Please be aware of viruses and malwares
## For mobile
### Install
1. In the [Releases tab](https://gitea.com/SweetSea-ButImNotSweet/tromi_mobile/releases) and click on ``Source code (zip)``
2. Extract the folder inside to somewhere you can easy to navigate
3. Open the folder ``tromi_mobile``, select all files, then add to a ``zip`` archive and pick a name that easy to remember, example: ``tromi_moblie.zip``
4. Rename the file extension, from ``zip`` to ``love`` (``tromi_mobile.zip`` to ``tromi_mobile.love``)
5. Download [``LÖVE for Android``](https://love2d.org) (You should get version 11.5)
6. Install love2d engine (extra steps may require, you can read [this article from WikiHow](https://www.wikihow.com/Install-APK-Files-on-Android) if this is the first time for you to install app from APK file)
7. Now, press Home (or back to app launcher), you can see two icons: ``LÖVE for Android`` and ``LÖVE Launcher``. You should use the ``LÖVE Launcher`` to launch Tromi
### Launch
1. Launch ``LÖVE Launcher``, an file picker will appear
2. Navigate to where you put the file and pick Tromi
3. Pick the file and enjoy.

## For TV
> :warning: Before I tell you how, I need to tell you something
> I don't really recommended playing Tromi on TV if you ***don't want*** your electricity bill to be a slap in the face.<br>
> If you can, **plug in a keyboard or gamepad** and play the original Tromi instead of this one.<br>
> You can still use the remote, but remember that it ***is not designed for gaming***. And if you lose before the speed hits 20G, I implore you ***not to throw the remote at your TV and then curse me because your performance is worse than when you play on the keyboard***.
### Installion
1. On other device, do the step 1 - 5 in ``Install for mobile`` part
2. Install this file manager on your TV: https://play.google.com/store/apps/details?id=com.alphainventor.filemanager. Once you installed, you should see [this icon](https://play-lh.googleusercontent.com/9PzlG2XGr5sQDf5925tlZhqluOreI2cwzp-FOZiuj545Kt1Gk5EE9J4IdGsD7e5xWw=w240-h480-rw)
3. Opening the application, click on ``Access from...`` (network/other devices/etc.)
4. Transfer 2 downloaded files to TV
5. Install LÖVE for Android application (see step 6 in ``Install for mobile`` section)
6. *(First time only)* Open the ``tromi_mobile.love``. When the file manager asks you to open the file as what. Select ``Other``, then select ``LÖVE for Android`` and select ``Always``
7. Once you are in the key configuration scene, type ``88663366``. An predefined keybind will be set automatically, but you can now make a different one if you'd like to

### Launch
Navigate to where you put ``tromi_mobile.love`` in the File manager (the one you downloaded from the link in Install for TV section), just opening and Tromi should be launched.

# Changes
> I must make this list to follow the used license (GNU GPL v3)<br>
> :no_entry: There are ***very much*** breaking changes right now, and I can't always finish this list. I may try hard to do it.
* **NO DIFFERENCES IN GAMEPLAY**
* Files will be saved into ``Android/data/org.love2d.android/tromi_mobile`` instead the location where the game files in
* Add replay importer / exporter through clipboard
* UI are re-designed to compactible with touch screen
* Add on-screen buttons
* Replaced icons for 3 direction buttons (Left, Down, Right), using from Techmino's font.
* Add a special pre-made keybind for Android TV (only supports TV models have their remote has numerical area (0-9)).
    * To use it, you have to activate TV mode. Type ``88663366`` by using your remote or external keyboard at the "Control method selection screen" (Tromi will show this screen at the first boot). After typing, Tromi will automatically use that keybind.
    * To deactivate TV mode, press ``........`` (8 dots). ***WARNING:*** the keyboard method will be resetted, which mean if you have assigned another keybind for keyboard, it will also be erased.
* Change the way to input secret code to activate Pentominoes mode.
* Add a loading screen
* Update ``binser`` library, this fixes some weird bugs related to saving
* Add ``simple-button`` module, made by me (SweetSea)
* Replaced old Cambridge's ``config`` module with the new one inspired by "the sequel of Techmino"s ``SETTINGS`` module".
* Using `lily` to improve the loading speed of animated backgrounds and sounds.

# TODO
- [x] Add a way to export replay data for Android 11+
- [x] Revert ``bitser`` with ``binser`` (if and only if I can make it works)
- [x] Design a new on-screen buttons skin (the current one is come from [C₂₉H₂₅N₃O₅](https://github.com/C29H25N3O5), I am aware that it's not fit to Tromi's design language)
- [x] Updating on-screen control configuration screen
- [x] (Low priority) Design a new menu screen
- [x] Add shortcut to open replay converter at https://sweetsea-butimnotsweet.github.io/tromi_replay_converter
- [x] Redesign the replay selector screen

- [ ] Add user data management
- [ ] Redesign "the Control method selection" screen

# License (GNU GPLv3)
Please read ``COPYING.txt`` for more information.<br>

A small note about the music:
> Only mycophobia right now having the permission to music from Jerry Martin, I haven't got.<br>
>
> I sent an email about this issue (not mentioning about this game), but I haven't got any replies from them (I checked both my main inbox and spam too, nothing)<br>
>
> Both me and mycophobia can't give you a sub-license if you ask. What can I do is suggesting you to send an email about this (there is a chance that you may not receive any replies, but it's better than do nothing)

# Special thanks
* mycophobia for writing the original Tromi
* MrZ_26 for the base of ``VCTRL`` module (yea I stole from him his code ;-; )
* C₂₉H₂₅N₃O₅ for his virtual key design (used while during inital development), and icon font (from Techmino).

# Don't forget to check
* [Original Tromi](https://mycophobia.org/tromi)
* [Leaderboard](https://mycophobia.org/forums/viewtopic.php?t=29)

* [Cambridge](https://github.com/Cambridge-stacker/Cambridge)
* [Techmino](https://github.com/26F-Studio/Techmino)