function copy(t)
    -- returns deep copy of t (as opposed to the shallow copy you get from var = t)
    if type(t) ~= "table" then return t end
    local meta = getmetatable(t)
    local target = {}
    for k, v in pairs(t) do target[k] = v end
    setmetatable(target, meta)
    return target
end

function strTrueValues(tbl)
    -- returns a concatenation of all the keys in tbl with value true, separated with spaces
    str = ""
    for k, v in pairs(tbl) do
        if v == true then
            str = str .. k .. " "
        end
    end
    return str
end

function frameTime(min, sec, hth)
    -- returns a time in frames from a time in minutes-seconds-hundredths format
    if min == nil then min = 0 end
    if sec == nil then sec = 0 end
    if hth == nil then hth = 0 end
    return min*3600 + sec*60 + math.ceil(hth * 0.6)
end

function vAdd(v1, v2)
    -- returns the sum of vectors v1 and v2
    return {
        x = v1.x + v2.x,
        y = v1.y + v2.y
    }
end

function vNeg(v)
    -- returns the opposite of vector v
    return {
        x = -v.x,
        y = -v.y
    }
end

function formatTime(frames)
    -- returns a mm:ss:hh (h=hundredths) representation of the time in frames given 
    if frames < 0 then return formatTime(0) end
    local min, sec, hund
    min  = math.floor(frames/3600)
    sec  = math.floor(frames/60) % 60
    hund = math.floor(frames/.6) % 100
    str = string.format("%02d:%02d.%02d", min, sec, hund)
    return str
end

function formatBigNum(number)
    -- returns a string representing a number with commas as thousands separator (e.g. 12,345,678)
    local s
    if type(number) == "number" then
        s = string.format("%d", number)
    elseif type(number) == "string" then
        if not tonumber(number) then
            return
        else
            s = number
        end
    else
        return
    end
    local pos = math.mod1(string.len(s), 3)
    return string.sub(s, 1, pos)
        .. string.gsub(string.sub(s, pos+1), "(...)", ",%1")
end

function math.clamp(x, min, max)
    if max < min then
        min, max = max, min
    end
    return x < min and min or (x > max and max or x)
end

-- Returns a number congruent to n modulo m in the range [1;m] (as opposed to [0;m-1])
function math.mod1(n, m)
    return ((n-1) % m) + 1
end

---Round a number with specified unit
---@param n number
---@param u? number|1
---@return number
function math.roundUnit(n,u)
    local u = u or 1
    return math.floor(n/u+.5)*u
end

---@return table
function table.pack(...)
    local t = {...}
    t.n = select('#', ...)
    return t
end

---@param t1 table
---@param t2 table
---@return table t
---Merge 2 tables into one<br>(**WARNING**: t2 can **overwrite** some value of t1 if both tables have some same keys!)
function table.merge(t1,t2)
    local t = {}
    for k, v in pairs(t1) do t[k] = v end
    for k, v in pairs(t2) do t[k] = v end
    return t
end

function table.contains(table, element)
    for _, value in pairs(table) do
          if value == element then
            return true
          end
    end
    return false
end

function table.highest(table)
	local i = 1
	local highest = nil
	for key, value in pairs(table) do
		if highest == nil then highest = key end
		if table[key] > table[highest] then highest = key end
	end
	return highest
end

function table.lowest(table)
	local i = 1
	local lowest = nil
	for key, value in pairs(table) do
		if lowest == nil then lowest = key end
		if table[key] < table[lowest] then lowest = key end
	end
	return lowest
end

function drawText(text, x, y, size, orientation, color)
	if color == nil then color = {1, 1, 1, 1} end
	love.graphics.setFont(FONT_tromi)
    love.graphics.setColor(0, 0, 0, 0.8)
    love.graphics.printf(text, x+1, y+1, size*2, orientation, nil, 0.50)
    love.graphics.setColor(color)
    love.graphics.printf(text, x, y, size*2, orientation, nil, 0.5)
end

function drawBigText(text, x, y, size, orientation, color)
	if color == nil then color = {1, 1, 1, 1} end
	love.graphics.setFont(FONT_big)
    love.graphics.setColor(0, 0, 0, 0.8)
    love.graphics.printf(text, x+1, y+1, size*2, orientation, nil, 0.50)
    love.graphics.setColor(color)
    love.graphics.printf(text, x, y, size*2, orientation, nil, 0.5)
end

function boolToInt(value)
	if value then return 1 else return 0 end
end

function pairsByKeysReverse (t, f)
	local a = {}
	for n in pairs(t) do table.insert(a, n) end
	table.sort(a, f)
	local i = #t+1      -- iterator variable
	local iter = function ()   -- iterator function
		i = i - 1
		if a[i] == nil then return nil
		else return a[i], t[a[i]]
		end
	end
	return iter
end

function split_string(inputstr, sep)
	if sep == nil then
			sep = "%s"
	end
	local t={}
	for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
			table.insert(t, str)
	end
	return t
end
